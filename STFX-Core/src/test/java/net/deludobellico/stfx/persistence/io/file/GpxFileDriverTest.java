package net.deludobellico.stfx.persistence.io.file;

import net.deludobellico.stfx.model.Activity;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class GpxFileDriverTest {
    private static final String GPX_FILE = "tracks/Marcha_los_7_Picos.gpx";
    private FileDriver driver;
    private ClassLoader classLoader;

    @Before
    public void setUp() throws Exception {
        driver = STFXFileType.GPX.getFileDriver();
        classLoader = getClass().getClassLoader();
    }

    @After
    public void tearDown() throws Exception {
        driver = null;
        classLoader = null;
    }


    @Test
    public void importActivities() throws Exception {
        InputStream is = classLoader.getResourceAsStream(GPX_FILE);
        List<Activity> activities = driver.importActivities(is);
        Assert.assertNotNull(activities);
    }


    @Test
    public void importActivitiesValidating() throws Exception {
        InputStream is = classLoader.getResourceAsStream(GPX_FILE);
        List<Activity> activities = driver.importActivities(is, true);
        Assert.assertNotNull(activities);
    }

}