package net.deludobellico.stfx.persistence.dao;

import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.model.UserProfile;
import net.deludobellico.stfx.persistence.PersistenceConfig;
import net.deludobellico.stfx.persistence.dao.jpa.JpaDao;
import net.deludobellico.stfx.persistence.io.file.FileDriver;
import net.deludobellico.stfx.persistence.io.file.STFXFileType;
import net.deludobellico.stfx.persistence.io.util.FileIO;
import net.deludobellico.stfx.util.SampleObjectFactory;
import org.aeonbits.owner.ConfigCache;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class LongDaoTest {
    private static final String GPX_FOLDER = "/tracks/";
    private FileDriver driver;
    private Dao dao;

    @Before
    public void setUp() {
        driver = STFXFileType.GPX.getFileDriver();
        PersistenceConfig config = ConfigCache.getOrCreate(PersistenceConfig.class);
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(config.persistenceUnitName());
        dao = new JpaDao();
        dao.setEntityManager(emf.createEntityManager());
    }

    @After
    public void tearDown() throws Exception {
        driver = null;
        dao = null;
    }

    @Test
    public void addActivities() throws Exception {
        UserProfile userProfile;
        List<UserProfile> userProfiles = dao.getAllUserProfiles();
        if (dao.getAllUserProfiles().isEmpty()) userProfile = SampleObjectFactory.createUserProfile();
        else userProfile = userProfiles.get(0);
        dao.addUserProfile(userProfile);

        File folder = FileIO.getFile(GPX_FOLDER);
        FilenameFilter filter = STFXFileType.GPX.getFileTypeFilter();
        List<Activity> activities = driver.importActivities(folder, filter, true);
        Assert.assertNotNull(activities);
        dao.addActivities(activities, userProfile);
        Assert.assertFalse(userProfile.getActivities().isEmpty());
        assert (userProfile.getActivities().size() == activities.size());
    }


}