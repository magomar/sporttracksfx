package net.deludobellico.stfx.persistence.dao;


import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.model.UserProfile;
import net.deludobellico.stfx.persistence.PersistenceConfig;
import net.deludobellico.stfx.persistence.dao.jpa.JpaDao;
import net.deludobellico.stfx.util.SampleObjectFactory;
import org.aeonbits.owner.ConfigCache;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class ShortDaoTest {
    private Dao dao;

    @Before
    public void setUp() {
        PersistenceConfig config = ConfigCache.getOrCreate(PersistenceConfig.class);
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(config.persistenceUnitName());
        dao = new JpaDao();
        dao.setEntityManager(emf.createEntityManager());
    }
    @After
    public void tearDown() throws Exception {
        dao = null;
    }

    @Test
    public void basicTest() throws Exception {
        UserProfile userProfile;
        List<UserProfile> userProfiles = dao.getAllUserProfiles();
        if (dao.getAllUserProfiles().isEmpty()) userProfile = SampleObjectFactory.createUserProfile();
        else userProfile = userProfiles.get(0);
        dao.addUserProfile(userProfile);

        Activity activity = SampleObjectFactory.createActivity();
        dao.addActivity(activity, userProfile);
        assert dao.getAllUserProfiles().contains(userProfile);
        assert userProfile.getActivities().contains(activity);

        userProfile.setBirthday(Date.valueOf(LocalDate.now()));


        dao.removeActivity(activity, userProfile);
        assert !userProfile.getActivities().contains(activity);

        userProfile.setCity("Valencia");

    }

}