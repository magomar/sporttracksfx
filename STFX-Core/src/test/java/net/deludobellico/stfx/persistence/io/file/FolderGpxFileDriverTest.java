package net.deludobellico.stfx.persistence.io.file;

import net.deludobellico.stfx.model.Activity;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class FolderGpxFileDriverTest {
//    private static final String GPX_FOLDER = "D:\\Google Drive\\Deporte y naturaleza\\GPX\\activities";
    private static final String GPX_FOLDER = "D:\\Documents\\Netbeans\\tracks";
    private FileDriver driver;

    @Before
    public void setUp() throws Exception {
        driver = STFXFileType.GPX.getFileDriver();
    }

    @After
    public void tearDown() throws Exception {
        driver = null;
    }

    @Test
    public void importActivitiesFromFolder() throws Exception {
        File folder = new File(GPX_FOLDER);
        FilenameFilter filter = STFXFileType.GPX.getFileTypeFilter();
        List<Activity> activities = driver.importActivities(folder, filter, true);
        Assert.assertNotNull(activities);
    }

}