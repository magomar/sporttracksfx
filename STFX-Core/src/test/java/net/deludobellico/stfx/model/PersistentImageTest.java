package net.deludobellico.stfx.model;

import javafx.scene.image.Image;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class PersistentImageTest {
    private static final String PNG_IMAGE = "images/default_avatar_male.png";

    @Test
    public void testPngImage() throws Exception {
        Image pngImage = new Image(getClass().getClassLoader().getResourceAsStream(PNG_IMAGE));
        PersistentImage pi = new PersistentImage(pngImage);
        Image img = pi.getImage();
        PersistentImage pi2 = new PersistentImage(img);
        assert Arrays.equals(pi.getData(),pi2.getData());
    }

}