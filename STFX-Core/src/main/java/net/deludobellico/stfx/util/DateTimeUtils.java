package net.deludobellico.stfx.util;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class DateTimeUtils {
    static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyy HH:mm");
    static final DatatypeFactory DATATYPE_FACTORY;

    static {
        try {
            DATATYPE_FACTORY = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new IllegalStateException("Error while trying to obtain a new instance of DatatypeFactory", e);
        }
    }

    private DateTimeUtils() {
    }

    public static LocalDate parseDate(String dateString) {
        return LocalDate.parse(dateString, DATE_FORMATTER);
    }

    public static LocalTime parseTime(String timeString) {
        return LocalTime.parse(timeString, TIME_FORMATTER);
    }

    public static LocalDateTime parseDateTime(String dateTimeString) {
        return LocalDateTime.parse(dateTimeString, DATE_TIME_FORMATTER);
    }

    public static String format(LocalDate date, LocalTime time) {
        return LocalDateTime.of(date, time).format(DATE_TIME_FORMATTER);
    }

    public static String format(LocalDateTime localDateTime) {
        return localDateTime.format(DATE_TIME_FORMATTER);
    }

    public static String format(LocalDate localDate) {
        return localDate.format(DATE_FORMATTER);
    }

    public static String format(LocalTime localTime) {
        return localTime.format(TIME_FORMATTER);
    }

    public static String format(Duration duration) {
        long hours = duration.getSeconds() / 3600;
        long minutes = (duration.getSeconds() % 3600) / 60;
        long seconds = duration.getSeconds() % 60;
        return String.format("%2d:%2d:%2d", hours, minutes, seconds);
    }

    public static LocalDateTime toLocalDateTime(XMLGregorianCalendar calendar) {
        if (calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().toZonedDateTime().toLocalDateTime();
    }

    public static LocalDate toLocalDate(XMLGregorianCalendar calendar) {
        if (calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().toZonedDateTime().toLocalDate();
    }

    public static LocalTime toLocalTime(XMLGregorianCalendar calendar) {
        if (calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().toZonedDateTime().toLocalTime();
    }

    public static XMLGregorianCalendar toXMLGregorianCalendar(LocalDate date) {
        GregorianCalendar gCalendar = new GregorianCalendar(date.getYear(), date.getMonthValue(), date.getDayOfMonth());
        XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(XMLGregorianCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
        return xmlCalendar;
    }

    public static XMLGregorianCalendar toXMLGregorianCalendar(LocalDate date, LocalTime time) {
        if (time == null) return toXMLGregorianCalendar(date);
        GregorianCalendar gCalendar = new GregorianCalendar(date.getYear(), date.getMonthValue(), date.getDayOfMonth(),
                time.getHour(), time.getMinute(), time.getSecond());
        XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(XMLGregorianCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
        return xmlCalendar;
    }

    public static XMLGregorianCalendar toXMLGregorianCalendar(LocalDateTime datetime) {
        GregorianCalendar gCalendar = new GregorianCalendar(datetime.getYear(), datetime.getMonthValue(),
                datetime.getDayOfMonth(), datetime.getHour(), datetime.getMinute(), datetime.getSecond());
        XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(XMLGregorianCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
        return xmlCalendar;
    }

    public static Date toDate(XMLGregorianCalendar calendar) {
        return calendar.toGregorianCalendar().getTime();
    }

    public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(date.getTime());
        return DATATYPE_FACTORY.newXMLGregorianCalendar(gc);
    }

    public static java.sql.Date toSqlDate(XMLGregorianCalendar calendar) {
        return java.sql.Date.valueOf(toLocalDate(calendar));
    }

    public static java.sql.Time toSqlTime(XMLGregorianCalendar calendar) {
        return java.sql.Time.valueOf(toLocalTime(calendar));
    }

    public static XMLGregorianCalendar toXMLGregorianCalendar(java.sql.Date date, java.sql.Time time) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(time);
        int hours = calendar.get(Calendar.HOUR);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hours);
        calendar.add(Calendar.MINUTE, minutes);
        calendar.add(Calendar.SECOND, seconds);
        return DATATYPE_FACTORY.newXMLGregorianCalendar((GregorianCalendar) calendar);
    }
}
