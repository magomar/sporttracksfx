package net.deludobellico.stfx.util;

import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.model.UserProfile;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 25/07/2016.
 */
public final class SampleObjectFactory {
    //TODO consider mock objects, some framework
    // see https://code.google.com/archive/p/atunit/

    public static UserProfile createUserProfile() {
        UserProfile userProfile = new UserProfile("testUser","1234");
        userProfile.setCountry("Spain");
        userProfile.setWeight(70.5);
        return userProfile;
    }

    public static Activity createActivity() {
        Activity activity = new Activity();
        return activity;
    }
}
