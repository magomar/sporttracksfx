package net.deludobellico.stfx.util;

import java.util.concurrent.TimeUnit;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class Stopwatch {
    private long start;
    private long stop;

    public void start() {
        start = System.nanoTime(); // start timing
        stop = start;
    }

    public void stop() {
        stop = System.nanoTime();
    }


    public long getTotalTime() {
        return stop - start;
    }

    public long getTimeFromLastStop() {
        return System.nanoTime() - stop;
    }

    @Override
    public String toString() {
        long milliseconds = TimeUnit.NANOSECONDS.toMillis(getTotalTime());
        return "   Time: " + Long.toString(milliseconds) + "ms"; // returns execution time
    }

    public void print() {
        System.out.println(toString());
    }

}
