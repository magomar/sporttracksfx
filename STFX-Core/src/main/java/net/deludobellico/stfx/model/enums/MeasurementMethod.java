package net.deludobellico.stfx.model.enums;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 25/07/2016.
 */
public enum MeasurementMethod implements I18nEnum {
    /**
     * Imperial units
     */
    IMPERIAL,
    /**
     * Metric
     */
    METRIC,
    /**
     * Unknown
     */
    UNKNOWN {
        @Override
        public String getMessageKey(Enum<?> e) {
            return "Common.UNKNOWN";
        }
    };}
