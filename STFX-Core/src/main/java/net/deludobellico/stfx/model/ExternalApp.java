package net.deludobellico.stfx.model;

import javafx.scene.image.Image;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 18/07/2016.
 */
public interface ExternalApp {
    String getName();
    String getDescription();
    Image getLogo();
}
