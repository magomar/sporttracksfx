package net.deludobellico.stfx.model;

import javax.persistence.Embeddable;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
@Embeddable
public class ActivityDataPoints {
    private int numPoints;
    /**
     * Geographic latitude in degrees. Negative values are south of the equator.
     */
    private double[] latitude;
    /**
     * Geographic longitude in degrees. Negative values are west of the Greenwich meridian.
     */
    private double[] longitude;
    /**
     * Elevation in meters (over the sea level)
     */
    private double[] elevation;
    /**
     * ratio between elevation and distance in meters (elevation / distance)
     */
    private double[] grade;
    /**
     * accumulated increment of altitude
     */
    private double[] ascent;
    /**
     * accumulated decrement of altitude
     */
    private double[] descend;
    /**
     * Total time in milliseconds from start
     */
    private int[] duration;
    /**
     * Total moving time in milliseconds from start
     */
    private int[] movingTime;
    /**
     * Distance travelled in meters
     */
    private double[] distance;
    /**
     * Speed in meters per second
     */
    private double[] speed;
    /**
     * Heart rate in bits per second
     */
    private int[] heartRate;
    /**
     * Cadence in strokes per minute
     */
    private int[] cadence;
    /**
     * Temperature in Celsius degrees
     */
    private double[] airTemp;
    /**
     * Power in wats
     */
    private int[] power;

    public ActivityDataPoints() {
        this.numPoints = 0;
    }

    public ActivityDataPoints(int numPoints) {
        this.numPoints = numPoints;
    }

    public int getNumPoints() {
        return numPoints;
    }

    public void setNumPoints(int numPoints) {
        this.numPoints = numPoints;
    }

    public double[] getLatitude() {
        return latitude;
    }

    public void setLatitude(double[] latitude) {
        this.latitude = latitude;
    }

    public double[] getLongitude() {
        return longitude;
    }

    public void setLongitude(double[] longitude) {
        this.longitude = longitude;
    }

    public double[] getElevation() {
        return elevation;
    }

    public void setElevation(double[] elevation) {
        this.elevation = elevation;
    }

    public double[] getGrade() {
        return grade;
    }

    public void setGrade(double[] grade) {
        this.grade = grade;
    }

    public double[] getAscent() {
        return ascent;
    }

    public void setAscent(double[] ascent) {
        this.ascent = ascent;
    }

    public double[] getDescend() {
        return descend;
    }

    public void setDescend(double[] descend) {
        this.descend = descend;
    }

    public int[] getDuration() {
        return duration;
    }

    public void setDuration(int[] duration) {
        this.duration = duration;
    }

    public int[] getMovingTime() {
        return movingTime;
    }

    public void setMovingTime(int[] movingTime) {
        this.movingTime = movingTime;
    }

    public double[] getDistance() {
        return distance;
    }

    public void setDistance(double[] distance) {
        this.distance = distance;
    }

    public double[] getSpeed() {
        return speed;
    }

    public void setSpeed(double[] speed) {
        this.speed = speed;
    }

    public int[] getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int[] heartRate) {
        this.heartRate = heartRate;
    }

    public int[] getCadence() {
        return cadence;
    }

    public void setCadence(int[] cadence) {
        this.cadence = cadence;
    }

    public double[] getAirTemp() {
        return airTemp;
    }

    public void setAirTemp(double[] airTemp) {
        this.airTemp = airTemp;
    }

    public int[] getPower() {
        return power;
    }

    public void setPower(int[] power) {
        this.power = power;
    }
}
