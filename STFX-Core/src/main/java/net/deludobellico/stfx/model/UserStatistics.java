package net.deludobellico.stfx.model;

import javax.persistence.Entity;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 * Created on 14/07/2016.
 */
@Entity
public class UserStatistics {
    /**
     * distance (in metres) of user's longest ride
     */
    private double biggestRideDistance;
    /**
     * elevation gain (in metres) of user's biggest climb
     */
    private double biggestAscent;
    /**
     * Statistics for last 28 days' rides
     */
    private StatisticsEntry recentRideTotals;
    /**
     * Statistics for last 28 days' runs
     */
    private StatisticsEntry recentRunTotals;
    /**
     * Statistics for last 28 days' swims
     */
    private StatisticsEntry recentSwimTotals;
    /**
     * Year to date ride statistics
     */
    private StatisticsEntry ytdRideTotals;
    /**
     * Year to date run statistics
     */
    private StatisticsEntry ytdRunTotals;
    /**
     * Year to date swim statistics
     */
    private StatisticsEntry ytdSwimTotals;
    /**
     * All time ride statistics
     */
    private StatisticsEntry allRideTotals;
    /**
     * All time run statistics
     */
    private StatisticsEntry allRunTotals;
    /**
     * All time swim statistics
     */
    private StatisticsEntry allSwimTotals;

    /**
     * @return the allRideTotals
     */
    public StatisticsEntry getAllRideTotals() {
        return this.allRideTotals;
    }
    /**
     * @return the allRunTotals
     */
    public StatisticsEntry getAllRunTotals() {
        return this.allRunTotals;
    }
    /**
     * @return the allSwimTotals
     */
    public StatisticsEntry getAllSwimTotals() {
        return this.allSwimTotals;
    }
    /**
     * @return the biggestAscent
     */
    public double getBiggestAscent() {
        return this.biggestAscent;
    }
    /**
     * @return the biggestRideDistance
     */
    public double getBiggestRideDistance() {
        return this.biggestRideDistance;
    }
    /**
     * @return the recentRideTotals
     */
    public StatisticsEntry getRecentRideTotals() {
        return this.recentRideTotals;
    }
    /**
     * @return the recentRunTotals
     */
    public StatisticsEntry getRecentRunTotals() {
        return this.recentRunTotals;
    }
    /**
     * @return the recentSwimTotals
     */
    public StatisticsEntry getRecentSwimTotals() {
        return this.recentSwimTotals;
    }
    /**
     * @return the ytdRideTotals
     */
    public StatisticsEntry getYtdRideTotals() {
        return this.ytdRideTotals;
    }
    /**
     * @return the ytdRunTotals
     */
    public StatisticsEntry getYtdRunTotals() {
        return this.ytdRunTotals;
    }
    /**
     * @return the ytdSwimTotals
     */
    public StatisticsEntry getYtdSwimTotals() {
        return this.ytdSwimTotals;
    }

    /**
     * @param allRideTotals the allRideTotals to set
     */
    public void setAllRideTotals(final StatisticsEntry allRideTotals) {
        this.allRideTotals = allRideTotals;
    }
    /**
     * @param allRunTotals the allRunTotals to set
     */
    public void setAllRunTotals(final StatisticsEntry allRunTotals) {
        this.allRunTotals = allRunTotals;
    }
    /**
     * @param allSwimTotals the allSwimTotals to set
     */
    public void setAllSwimTotals(final StatisticsEntry allSwimTotals) {
        this.allSwimTotals = allSwimTotals;
    }
    /**
     * @param biggestAscent the biggestAscent to set
     */
    public void setBiggestAscent(final double biggestAscent) {
        this.biggestAscent = biggestAscent;
    }
    /**
     * @param biggestRideDistance the biggestRideDistance to set
     */
    public void setBiggestRideDistance(final double biggestRideDistance) {
        this.biggestRideDistance = biggestRideDistance;
    }
    /**
     * @param recentRideTotals the recentRideTotals to set
     */
    public void setRecentRideTotals(final StatisticsEntry recentRideTotals) {
        this.recentRideTotals = recentRideTotals;
    }
    /**
     * @param recentRunTotals the recentRunTotals to set
     */
    public void setRecentRunTotals(final StatisticsEntry recentRunTotals) {
        this.recentRunTotals = recentRunTotals;
    }
    /**
     * @param recentSwimTotals the recentSwimTotals to set
     */
    public void setRecentSwimTotals(final StatisticsEntry recentSwimTotals) {
        this.recentSwimTotals = recentSwimTotals;
    }
    /**
     * @param ytdRideTotals the ytdRideTotals to set
     */
    public void setYtdRideTotals(final StatisticsEntry ytdRideTotals) {
        this.ytdRideTotals = ytdRideTotals;
    }
    /**
     * @param ytdRunTotals the ytdRunTotals to set
     */
    public void setYtdRunTotals(final StatisticsEntry ytdRunTotals) {
        this.ytdRunTotals = ytdRunTotals;
    }
    /**
     * @param ytdSwimTotals the ytdSwimTotals to set
     */
    public void setYtdSwimTotals(final StatisticsEntry ytdSwimTotals) {
        this.ytdSwimTotals = ytdSwimTotals;
    }
}
