package net.deludobellico.stfx.model;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import javax.persistence.Embeddable;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
@Embeddable
public class PersistentImage {
    private byte[] data;

    public PersistentImage(Image image)  {
        java.awt.image.BufferedImage buffImg = SwingFXUtils.fromFXImage(image, null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(buffImg, "png", baos);
            data = baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Image getImage() {
        BufferedImage imag = null;
        try {
            imag = ImageIO.read(new ByteArrayInputStream(data));
            return SwingFXUtils.toFXImage(imag, null);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public byte[] getData() {
        return data;
    }

}