package net.deludobellico.stfx.model;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 14/07/2016.
 */
public class StatisticsEntry {
    /**
     * Number of activities
     */
    private int count;
    /**
     * Total distance in metres
     */
    private double distance;
    /**
     * Total moving time in seconds (excluding time spent stopped)
     */
    private int movingTime;
    /**
     * Total elapsed time in seconds (including time spent stopped)
     */
    private int duration;
    /**
     * Total elevation gain in metres
     */
    private double ascent;
    /**
     * Total number of achievements
     */
    private int achievementCount;

    /**
     * @return the achievementCount
     */
    public int getAchievementCount() {
        return this.achievementCount;
    }
    /**
     * @return the count
     */
    public int getCount() {
        return this.count;
    }
    /**
     * @return the distance
     */
    public double getDistance() {
        return this.distance;
    }
    /**
     * @return the duration
     */
    public int getDuration() {
        return this.duration;
    }
    /**
     * @return the ascent
     */
    public double getAscent() {
        return this.ascent;
    }
    /**
     * @return the movingTime
     */
    public int getMovingTime() {
        return this.movingTime;
    }

    /**
     * @param achievementCount the achievementCount to set
     */
    public void setAchievementCount(final int achievementCount) {
        this.achievementCount = achievementCount;
    }
    /**
     * @param count the count to set
     */
    public void setCount(final int count) {
        this.count = count;
    }
    /**
     * @param distance the distance to set
     */
    public void setDistance(final double distance) {
        this.distance = distance;
    }
    /**
     * @param duration the duration to set
     */
    public void setDuration(final int duration) {
        this.duration = duration;
    }
    /**
     * @param ascent the ascent to set
     */
    public void setAscent(final double ascent) {
        this.ascent = ascent;
    }
    /**
     * @param movingTime the movingTime to set
     */
    public void setMovingTime(final int movingTime) {
        this.movingTime = movingTime;
    }
}
