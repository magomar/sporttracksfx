package net.deludobellico.stfx.model;

import net.deludobellico.stfx.model.enums.MeasurementMethod;
import net.deludobellico.stfx.model.enums.Sex;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
@Entity
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = UserProfile.findAll, query = "SELECT u from UserProfile u")
})
public class UserProfile {
    public final static String PREFIX = "UserProfile";
    public final static String findAll = PREFIX + ".findAll";

    @Id
    @GeneratedValue
    private int id;
    /**
     * User's name used to log-in
     */
    private String username = "";
    /**
     * User's password used to log-in
     */
    private String password ="";
    /**
     * DB persisted image representing the user
     */
    private PersistentImage avatar = null;
    /**
     * User's sex
     */
    private Sex sex = Sex.UNKNOWN;
    /**
     * User's date of birth
     */
    private Date birthday = null;
    /**
     * User's weight (in kilograms)
     */
    private double weight = 0.0;
    /**
     * User's first name
     */
    private String firstname = "";
    /**
     * User's last name
     */
    private String lastname = "";
    /**
     * City the User lives in
     */
    private String city = "";
    /**
     * State, county, canton etc that the User lives in
     */
    private String state = "";
    /**
     * Country the User lives in
     */
    private String country = "";
    /**
     * User's email address
     */
    private String email ="";
    /**
     * Date format preference
     */
    private String datePreference ="dd/MM/yyy HH:mm";
    /**
     * Measurement preference (metric or imperial)
     */
    private MeasurementMethod measurementPreference = MeasurementMethod.METRIC;


    @OneToMany(orphanRemoval = true)
    private List<Activity> activities = new ArrayList<>();

    @OneToMany(orphanRemoval = true)
    private Map<String, UserAccount> accounts = new HashMap<>();

    public UserProfile(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PersistentImage getAvatar() {
        return avatar;
    }

    public void setAvatar(PersistentImage avatar) {
        this.avatar = avatar;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDatePreference() {
        return datePreference;
    }

    public void setDatePreference(String datePreference) {
        this.datePreference = datePreference;
    }

    public MeasurementMethod getMeasurementPreference() {
        return measurementPreference;
    }

    public void setMeasurementPreference(MeasurementMethod measurementPreference) {
        this.measurementPreference = measurementPreference;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public Map<String, UserAccount> getAccounts() {
        return accounts;
    }

    public UserAccount getAccount(ExternalApp externalApp) {
        return accounts.get(externalApp.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserProfile that = (UserProfile) o;

        return username.equals(that.username);

    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "username='" + username + '\'' +
                ", id=" + id +
                '}';
    }
}
