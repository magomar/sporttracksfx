package net.deludobellico.stfx.model.enums;

/**
 * <p>
 * These are all the available Activity types:
 * </p>
 * <p>
 * <ul>
 * <li>Ride</li>
 * <li>Run</li>
 * <li>Swim</li>
 * <li>Hike</li>
 * <li>Walk</li>
 * <li>AlpineSki</li>
 * <li>BackcountrySki</li>
 * <li>Caneoing</li>
 * <li>Crossfit</li>
 * <li>EBikeRide</li>
 * <li>Elliptical</li>
 * <li>IceSkate</li>
 * <li>InlineSkate</li>
 * <li>Kayaking</li>
 * <li>Kitesurf</li>
 * <li>NordicSki</li>
 * <li>RockClimbing</li>
 * <li>RollerSki</li>
 * <li>Rowing</li>
 * <li>Snowboard</li>
 * <li>Snowshoe</li>
 * <li>StairStepper</li>
 * <li>StandUpPaddling</li>
 * <li>Surfing</li>
 * <li>VirtualRide
 * <li>WeightTraining</li>
 * <li>Windsurf</li>
 * <li>Workout</li>
 * <li>Yoga</li>
 * </ul>
 * <p>
 * <p>
 * Activities that don’t use real GPS should utilize the {@link #VIRTUAL_RIDE}
 * type. Electronically assisted rides should use the {@link #EBIKE_RIDE} type.
 * The @link {@link #WORKOUT} type is recommended for miscellaneous activities.
 * </p>
 *
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 15/07/2016.
 */
public enum ActivityType implements I18nEnum{
    /**
     * Bike ride
     */
    RIDE("ride"),
    /**
     * Run
     */
    RUN("run"),
    /**
     * Swim
     */
    SWIM("swim"),
    /**
     * Hike
     */
    HIKE("hike"),
    /**
     * Walk
     */
    WALK("walk"),
    /**
     * Alpine skiing
     */
    ALPINE_SKI("alpineski"),
    /**
     * Back-country skiing (off piste)
     */
    BACKCOUNTRY_SKI("backcountryski"),
    /**
     * Canoeing
     */
    CANOEING("canoeing"),
    /**
     * Crossfit
     */
    CROSSFIT("crossfit"),
    /**
     * E-Bike Ride
     */
    EBIKE_RIDE("ebikeride"),
    /**
     * Elliptical Trainer
     */
    ELLIPTICAL("elliptical"),
    /**
     * Ice skating
     */
    ICE_SKATE("iceskate"),
    /**
     * Inline skating (rollerblading)
     */
    INLINE_SKATE("inlineskate"),
    /**
     * Kayaking
     */
    KAYAKING("kayaking"),
    /**
     * Kite surfing
     */
    KITESURF("kitesurf"),
    /**
     * Nordic skiing (telemark)
     */
    NORDIC_SKI("nordicski"),
    /**
     * Rock climbing
     */
    ROCK_CLIMBING("rockclimbing"),
    /**
     * Rollerskiing
     */
    ROLLERSKI("rollerski"),
    /**
     * Rowing
     */
    ROWING("rowing"),
    /**
     * Snowboarding
     */
    SNOWBOARD("snowboard"),
    /**
     * Snowshoeing
     */
    SNOWSHOE("snowshoe"),
    /**
     * Stair stepper
     */
    STAIR_STEPPER("stairstepper"),
    /**
     * Stand-up Paddling
     */
    STAND_UP_PADDLING("standuppaddling"),
    /**
     * Surfing
     */
    SURFING("surfing"),
    /**
     * Virtual ride
     */
    VIRTUAL_RIDE("virtualride"),
    /**
     * Weight training
     */
    WEIGHT_TRAINING("weighttraining"),
    /**
     * Windsurfing
     */
    WINDSURF("windsurf"),
    /**
     * Workout
     */
    WORKOUT("workout"),
    /**
     * Yoga
     */
    YOGA("yoga"),
    /**
     * <p>
     * Should never occur but may if Strava API behaviour has changed
     * </p>
     */
    UNKNOWN("unknown") {
        @Override
        public String getMessageKey(Enum<?> e) {
            return "Common.UNKNOWN";
        }
    };

    /**
     * Description
     */
    private String description;

    ActivityType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

}
