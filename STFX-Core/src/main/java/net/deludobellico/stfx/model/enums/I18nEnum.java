package net.deludobellico.stfx.model.enums;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 * Created on 25/07/2016.
 */
public interface I18nEnum {

    default String getMessageKey(Enum<?> e) {
        return e.getClass().getSimpleName() + '.' + e.name().toLowerCase();
    }

}
