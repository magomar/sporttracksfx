package net.deludobellico.stfx.model.enums;

import java.util.EnumSet;
import java.util.Set;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public enum DataOption implements I18nEnum {
    GEOLOCATION,
    ELEVATION,
    TIME,
    HEART_RATE, //TrackPointExtension
    CADENCE, //TrackPointExtension
    AIR_TEMP, //TrackPointExtension
    WATER_TEMP, //TrackPointExtension
    DEPTH, //TrackPointExtension
    POWER,
    CALORIES; //PowerExtension

    public static final Set<DataOption> TCX_EXTENSION = EnumSet.range(HEART_RATE, DEPTH);
    public static final Set<DataOption> POWER_EXTENSION = EnumSet.of(POWER);
}
