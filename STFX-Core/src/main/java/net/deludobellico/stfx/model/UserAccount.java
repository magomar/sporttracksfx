package net.deludobellico.stfx.model;

import javax.persistence.Embeddable;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 18/07/2016.
 */
@Embeddable
public class UserAccount {
    private String app;
    private String username;
    private String password;
    private boolean connected;
    private boolean upload;
    private boolean download;

    public UserAccount(String app, String username, String password) {
        this.app = app;
        this.username = username;
        this.password = password;
        connected = false;
        upload = false;
        download = false;
    }

    public UserAccount(String app) {
        this(app,"","");
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public boolean isUpload() {
        return upload;
    }

    public void setUpload(boolean upload) {
        this.upload = upload;
    }

    public boolean isDownload() {
        return download;
    }

    public void setDownload(boolean download) {
        this.download = download;
    }
}
