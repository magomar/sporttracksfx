package net.deludobellico.stfx.model;

import net.deludobellico.stfx.model.enums.ActivityType;
import net.deludobellico.stfx.model.enums.DataOption;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Activity.findAll, query = "SELECT a FROM Activity a"),
        @NamedQuery(name = Activity.findById, query = "SELECT a FROM Activity a WHERE a.id = :id")
})
public class Activity implements Comparable<Activity>, Serializable {
    public final static String PREFIX = "Activity";
    public final static String findAll = PREFIX + ".findAll";
    public final static String findById = PREFIX + ".findById";

    @Id
    @GeneratedValue
    private int id;
    /**
     * The identifier given to the activity by other apps(eg. Strava, Endomondo, etc)
     */
    private Map<String, Integer> externalId = new HashMap<>();
    /**
     * Optional data found in this activity (eg. heart rate, cadence, temperature, power, etc.)
     */
    private Set<DataOption> options = EnumSet.noneOf(DataOption.class);
    /**
     * Date the activity was started
     */
    private java.sql.Date date;
    /**
     * Time the activity was started
     */
    private java.sql.Time time;
    /**
     * Name of the activity
     */
    private String name;
    /**
     * Detailed description of the activity
     */
    private String description;
    /**
     * Distance travelled in metres.
     */
    private double distance;
    /**
     * Total java.sql.Time including stopped java.sql.Time, in seconds
     */
    private int duration;
    /**
     * Total moving java.sql.Time in seconds.
     */
    private int movingTime;
    /**
     * Total elevation gain in metres
     */
    private double ascent;
    /**
     * Total elevation loss in metres
     */
    private double descend;
    /**
     * Average speed in Km/h
     */
    private double averageSpeed;
    /**
     * Maximum speed in Km/h (quite
     * often as a result of GPS inaccuracies).
     */
    private double maxSpeed;
    /**
     * Average RPM if cadence data was provided
     */
    private int averageCadence;
    /**
     * Max RPM if cadence data was provided
     */
    private int maxCadence;
    /**
     * Average heart rate (in beats per minute) if heart rate data was provided
     */
    private double averageHeartrate;
    /**
     * Min heart rate (in beats per minute) if heart rate data was provided
     */
    private int minHeartrate;
    /**
     * Maximum heart rate (in beats per minute) if heart rate data was provided
     */
    private int maxHeartrate;
    /**
     * Minimum elevation in meters
     */
    private double minElevation;
    /**
     * Maximum elevation in meters
     */
    private double maxElevation;
    /**
     * Average airTemp (in degrees Celsius) if airTemp data was provided
     */
    private double averageTemp;
    /**
     * Kilocalories expended
     */
    private double calories;
    /**
     * Average power (in watts) for rides only. S
     */
    private double averageWatts;
    /**
     * Weighted average power (in watts) for rides with power meter data only.
     */
    private double weightedAverageWatts;
    /**
     * The name of the device used to record the activity
     */
    private String deviceName;
    /**
     * Type of activity (eg. ride, run, skating, etc.)
     */
    private ActivityType type;
    /**
     * Data points obtained from a GPS device or inferred from it
     */
    private ActivityDataPoints data;


    public Activity(String name, Date date, Time time, ActivityType type) {
        options = EnumSet.noneOf(DataOption.class);
        this.name = name;
        this.date = date;
        this.time = time;
        this.type = type;
    }

    public Activity(String name, Date date, Time time) {
        this(name, date, time, ActivityType.UNKNOWN);
    }

    public Activity(String name) {
        this(name, Date.valueOf(LocalDate.now()), Time.valueOf(LocalTime.now()), ActivityType.UNKNOWN);
    }

    public Activity() {
        this(LocalDateTime.now().toString());
    }

    public Map<String, Integer> getExternalId() {
        return externalId;
    }

    public java.sql.Date getDate() {
        return date;
    }

    public void setDate(java.sql.Date date) {
        this.date = date;
    }

    public java.sql.Time getTime() {
        return time;
    }

    public void setTime(java.sql.Time time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getMovingTime() {
        return movingTime;
    }

    public void setMovingTime(int movingTime) {
        this.movingTime = movingTime;
    }

    public double getAscent() {
        return ascent;
    }

    public void setAscent(double ascent) {
        this.ascent = ascent;
    }

    public double getDescend() {
        return descend;
    }

    public void setDescend(double descend) {
        this.descend = descend;
    }

    public double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getAverageCadence() {
        return averageCadence;
    }

    public void setAverageCadence(int averageCadence) {
        this.averageCadence = averageCadence;
    }

    public int getMaxCadence() {
        return maxCadence;
    }

    public void setMaxCadence(int maxCadence) {
        this.maxCadence = maxCadence;
    }

    public double getAverageTemp() {
        return averageTemp;
    }

    public void setAverageTemp(double averageTemp) {
        this.averageTemp = averageTemp;
    }

    public double getAverageWatts() {
        return averageWatts;
    }

    public void setAverageWatts(double averageWatts) {
        this.averageWatts = averageWatts;
    }

    public double getWeightedAverageWatts() {
        return weightedAverageWatts;
    }

    public void setWeightedAverageWatts(double weightedAverageWatts) {
        this.weightedAverageWatts = weightedAverageWatts;
    }

    public double getAverageHeartrate() {
        return averageHeartrate;
    }

    public void setAverageHeartrate(double averageHeartrate) {
        this.averageHeartrate = averageHeartrate;
    }

    public int getMinHeartrate() {
        return minHeartrate;
    }

    public void setMinHeartrate(int minHeartrate) {
        this.minHeartrate = minHeartrate;
    }

    public int getMaxHeartrate() {
        return maxHeartrate;
    }

    public void setMaxHeartrate(int maxHeartrate) {
        this.maxHeartrate = maxHeartrate;
    }

    public double getMinElevation() {
        return minElevation;
    }

    public void setMinElevation(double minElevation) {
        this.minElevation = minElevation;
    }

    public double getMaxElevation() {
        return maxElevation;
    }

    public void setMaxElevation(double maxElevation) {
        this.maxElevation = maxElevation;
    }

    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Set<DataOption> getOptions() {
        return options;
    }

    public void setOptions(Set<DataOption> options) {
        this.options = options;
    }

    public ActivityDataPoints getData() {
        return data;
    }

    public void setData(ActivityDataPoints data) {
        this.data = data;
    }

    public ActivityType getType() {
        return type;
    }

    public void setType(ActivityType type) {
        this.type = type;
    }

    public LocalDate getLocalDate() {
        return date.toLocalDate();
    }

    public LocalTime getLocalTime() {
        return time.toLocalTime();
    }

    public LocalDateTime getLocalDateTime() {
        return LocalDateTime.of(date.toLocalDate(), time.toLocalTime());
    }

    @Override
    public int compareTo(Activity o) {
        int compareDate = date.compareTo(o.date);
        if (compareDate != 0) return compareDate;
        else return time.compareTo(o.time);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Activity activity = (Activity) o;

        if (!date.equals(activity.date)) return false;
        return time.equals(activity.time);
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + time.hashCode();
        return result;
    }

}
