package net.deludobellico.stfx.persistence.dao;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
@Deprecated
public enum PersistenceFramework {
    JPA("net.deludobellico.stfx.persistence.dao.jpa.JpaDao");
    private final String className;

    PersistenceFramework(String className) {
        this.className = className;
    }

    public Class<? extends Dao> getDaoClass() throws ClassNotFoundException {
        Class<? extends Dao> clazz = (Class<? extends Dao>) Class.forName(className);
        return clazz;
    }
}
