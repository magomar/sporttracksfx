package net.deludobellico.stfx.persistence.io.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 * This class controls all access to disk and stores resources paths.
 */
public class FileIO {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileIO.class);
    private static final boolean IS_DEPLOYED;

    static {
        Path userPath = FileSystems.getDefault().getPath(System.getProperty("user.dir"));
        IS_DEPLOYED = userPath.endsWith("bin");
        LOGGER.info("User dir = {} -- Is deployed? {}", userPath.toString(), IS_DEPLOYED);
    }

    /**
     * Protect the class from being instantiated
     */
    private FileIO() {
    }

    public static boolean isDeployed() {
        return IS_DEPLOYED;
    }

    /**
     * Gets the list of files in a given directory, using certain filter, and with the option to search the
     * directory recursively.
     */

    public static List<File> listFiles(File directory, FilenameFilter filter, boolean recursively) {
        List<File> files = new ArrayList<>();
        if (directory.isDirectory()) {
            File[] entries = directory.listFiles();
            for (File entry : entries) {
                if (filter == null || filter.accept(directory, entry.getName())) {
                    files.add(entry);
                }
                if (recursively && entry.isDirectory()) {
                    files.addAll(listFiles(entry, filter, recursively));
                }
            }
        } else {
            LOGGER.error("Error listing files. {} is not a directory", directory);
        }
        return files;
    }

    /**
     * Gets a file given a string description of a relative path, or various relative paths to be appended in order
     *
     * @param relativePath
     * @return
     */
    public static File getFile(String... relativePath) {
        Path path = getPath(relativePath);
        return getFileOrCreateNew(path.toString());
    }

    /**
     * Gets a file given an absolute path, or creates a new file if no file exists in that path
     *
     * @param path
     * @return
     */
    private static File getFileOrCreateNew(String path) {
        File f = null;
        try {
            f = new File(path);
            boolean exists = f.exists();
            if (!exists) {
                boolean createNew = f.createNewFile();
                if (!createNew) {
                    throw new IOException();
                }
            }
        } catch (IOException e) {
            LOGGER.warn("Error creating file {}" + path);
        }
        return f;
    }

    /****
     * Gets an absolute path given a string description of a relative path, or various relative paths to be appended in order
     *
     * @param relativePath
     * @return
     */
    public static Path getPath(String... relativePath) {
        String[] paths;
        if (IS_DEPLOYED) {
            int numPaths = relativePath.length + 1;
            paths = new String[numPaths];
            paths[0] = "..";
            for (int i = 1; i < paths.length; i++) {
                paths[i] = relativePath[i - 1];
            }
        } else {
            paths = relativePath;
        }
        return FileSystems.getDefault().getPath(System.getProperty("user.dir"), paths);
    }

//    public static Path getResourcesPath(String secondary, String... relativePath) {
//        String[] paths = new String[relativePath.length + 2];
//        paths[0] = secondary;
//        paths[1] = FileIO.RESOURCES_FOLDER;
//        for (int i = 2; i < paths.length; i++) {
//            paths[i] = relativePath[i - 2];
//        }
//        return getPath(paths);
//    }

    /**
     * Copy one file to another
     *
     * @param sourceFile
     * @param targetFile
     */
    public static void copy(File sourceFile, File targetFile) {
        try {
            Files.copy(sourceFile.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes file extension from a string description of a path
     *
     * @param filePath
     * @return
     */
    public static String removeExtention(String filePath) {
        File f = new File(filePath);
        // if it's a directory there is no extension to remove
        if (f.isDirectory()) return filePath;
        String name = f.getName();
        final int lastPeriodPos = name.lastIndexOf('.');
        if (lastPeriodPos <= 0) {
            // No period after first character - return name as it was passed in
            return filePath;
        } else {
            // Remove the last period and everything after it
            File renamed = new File(f.getParent(), name.substring(0, lastPeriodPos));
            return renamed.getPath();
        }
    }

    /**
     * Gets the file extension of a given file
     *
     * @param file
     * @return
     */
    public static String getExtension(File file) {
        if (file.isDirectory()) return "";
        String fileName = file.getName();
//        return fileName.replaceAll("^.*\\.(.*)$", "$1");
        final int lastPeriodPos = fileName.lastIndexOf('.');
        if (lastPeriodPos <= 0) {
            return "";
        } else {
            return fileName.substring(lastPeriodPos + 1);
        }
    }


}