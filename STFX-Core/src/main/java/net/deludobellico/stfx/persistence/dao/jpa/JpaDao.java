package net.deludobellico.stfx.persistence.dao.jpa;

import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.model.UserProfile;
import net.deludobellico.stfx.persistence.PersistenceConfig;
import net.deludobellico.stfx.persistence.dao.Dao;
import net.deludobellico.stfx.persistence.dao.DaoException;
import org.aeonbits.owner.ConfigCache;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.*;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         <p>
 *         see https://gist.github.com/james-d/ab72b487b7b12bb7a3bc
 */
@Singleton
public class JpaDao implements Dao {
    private EntityTransaction et;

//    @PersistenceUnit (name = "STFX") // This is used in JavaSE environment
//    private EntityManagerFactory emf;

    // Injected database connection:
//    @PersistenceContext(name = "STFX") // This only works in JEE environment,
    private EntityManager em;


//    @Inject
//    public void setEntityManagerFactory(EntityManagerFactory emf) {
//        this.emf = emf;
//        em = emf.createEntityManager();
//        et = em.getTransaction();
//    }

    @Override
    @Inject
    public void setEntityManager(EntityManager em) {
        this.em = em;
        et = em.getTransaction();
    }

//    public JpaDao() {
//        PersistenceConfig config = ConfigCache.getOrCreate(PersistenceConfig.class);
//        String persistenceUnitName = config.persistenceUnitName();
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnitName);
//        em = emf.createEntityManager();
//        et = em.getTransaction();
//    }



    @Override
    public void addUserProfile(UserProfile userProfile) throws DaoException {
        try {
            et.begin();
            em.persist(userProfile);
            et.commit();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }

    @Override
    public void updateUserProfile(UserProfile userProfile) throws DaoException {
        try {
            et.begin();
            em.merge(userProfile);
            et.commit();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }

    @Override
    public void removeUserProfile(UserProfile userProfile) throws DaoException {
        try {
            et.begin();
            em.remove(userProfile);
            et.commit();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }

    @Override
    public List<UserProfile> getAllUserProfiles() throws DaoException {
        return execute(UserProfile.findAll, UserProfile.class);
    }

    @Override
    public void addActivities(List<Activity> activities, UserProfile userProfile) throws DaoException {
        try {
            et.begin();
            if (!em.contains(userProfile)) em.persist(userProfile);
            for (Activity activity : activities) {
                em.persist(activity);
            }
            userProfile.getActivities().addAll(activities);
            et.commit();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }

    @Override
    public void updateActivity(Activity activity) throws DaoException {
        try {
            et.begin();
            em.merge(activity);
            et.commit();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }

    @Override
    public void addActivity(Activity activity, UserProfile userProfile) throws DaoException {
        try {
            et.begin();
            if (!em.contains(userProfile)) em.persist(userProfile);
            userProfile.getActivities().add(activity);
            em.persist(activity);
            et.commit();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }

    @Override
    public void removeActivity(Activity activity, UserProfile userProfile) throws DaoException {
        try {
            et.begin();
//            em.remove(activity);
            userProfile.getActivities().remove(activity);
            et.commit();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }

    public void save() {
        et.begin();
        em.flush();
        et.commit();
    }

    public void clear() {
        et.begin();
        em.flush();
        em.clear();
        et.commit();
    }

    public void close() {
        et.begin();
        et.commit();
        em.close();
    }

    private <T> List<T> execute(String queryName, Class<T> type) throws DaoException {
        try {
            TypedQuery<T> query = em.createNamedQuery(queryName, type);
            return query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }
}
