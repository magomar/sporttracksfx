
package net.deludobellico.stfx.persistence.jaxb.tcx;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Cadence_t complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Cadence_t">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Target_t">
 *       &lt;sequence>
 *         &lt;element name="Low" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="High" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cadence_t", namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", propOrder = {
    "low",
    "high"
})
public class CadenceT
    extends TargetT
{

    @XmlElement(name = "Low", namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")
    protected double low;
    @XmlElement(name = "High", namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")
    protected double high;

    /**
     * Obtiene el valor de la propiedad low.
     * 
     */
    public double getLow() {
        return low;
    }

    /**
     * Define el valor de la propiedad low.
     * 
     */
    public void setLow(double value) {
        this.low = value;
    }

    /**
     * Obtiene el valor de la propiedad high.
     * 
     */
    public double getHigh() {
        return high;
    }

    /**
     * Define el valor de la propiedad high.
     * 
     */
    public void setHigh(double value) {
        this.high = value;
    }

}
