package net.deludobellico.stfx.persistence.io.file;

import net.deludobellico.stfx.persistence.jaxb.JAXBTools;

import javax.xml.validation.Validator;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public interface FileType {

    String[] getFileExtension();

    Class getXmlRootClass();

    FileTypeFilter getFileTypeFilter();

    String getDescription();

    Validator getValidator();

    JAXBTools getJaxbTools();

    FileDriver getFileDriver();

    FileType fromFileExtension(String extension);

//    FileType fromFileExtension(String fileExtension);

}
