package net.deludobellico.stfx.persistence.io.file;

import net.deludobellico.stfx.analysis.ActivityStep;
import net.deludobellico.stfx.analysis.DistanceFunction;
import net.deludobellico.stfx.analysis.GPSDistanceFunction;
import net.deludobellico.stfx.model.*;
import net.deludobellico.stfx.model.enums.DataOption;
import net.deludobellico.stfx.persistence.io.IOConfig;
import net.deludobellico.stfx.persistence.jaxb.gpx.*;
import net.deludobellico.stfx.util.DateTimeUtils;
import org.aeonbits.owner.ConfigCache;

import javax.xml.bind.JAXBElement;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class GpxConverter {
    private static final DistanceFunction DISTANCE_FUNCTION;
    private static final double SPEED_THRESHOLD;
    private static final double DISTANCE_THRESHOLD;

    static {
        IOConfig config = ConfigCache.getOrCreate(IOConfig.class);
        DISTANCE_FUNCTION = GPSDistanceFunction.valueOf(config.distanceFunctionName());
        SPEED_THRESHOLD = config.speedThreshold();
        DISTANCE_THRESHOLD = config.distanceThreshold();
    }

    static public List<Activity> convert(GpxType gpx) {
        List<Activity> activities = new ArrayList<>(gpx.getTrk().size());
        for (TrkType trk : gpx.getTrk()) {
            activities.add(convert(trk, gpx.getMetadata()));
        }
        return activities;
    }

    static public Activity convert(TrkType track, MetadataType metadata) {

        WptType startPoint = track.getTrkseg().get(0).getTrkpt().get(0);
        Set<DataOption> options = findOutDataAvailable(startPoint);
        int numPoints = 0;
        for (TrksegType trksegType : track.getTrkseg()) {
            numPoints += trksegType.getTrkpt().size();
        }
        double[] latitude = new double[numPoints];
        double[] longitude = new double[numPoints];
        double[] elevation = new double[numPoints];
        double[] grade = new double[numPoints];
        double[] ascent = new double[numPoints];
        double[] descend = new double[numPoints];
        int[] duration = new int[numPoints];
        int[] movingTime = new int[numPoints];
        double[] distance = new double[numPoints];
        double[] speed = new double[numPoints];
        int[] heartRate = new int[numPoints];
        int[] cadence = new int[numPoints];
        double[] airTemp = new double[numPoints];
        int[] power = new int[numPoints];

        double maxSpeed = Double.MIN_VALUE;
        double minElevation = Double.MAX_VALUE;
        double maxElevation = Double.MIN_VALUE;
        int avgCadence = 0;
        int maxCadence = Integer.MIN_VALUE;
        int avgTemp = 0;
        int avgHearRate = 0;
        int minHeartRate = Integer.MAX_VALUE;
        int maxHeartRate = Integer.MIN_VALUE;
        double avgWatts = 0;

        ActivityStep step = new ActivityStep(startPoint.getLat().doubleValue(), startPoint.getLon().doubleValue(), 0);
        WptType w1 = startPoint;
        int p = 0;
        for (TrksegType segment : track.getTrkseg())
            for (WptType w2 : segment.getTrkpt()) {
                step = convert(w1, w2, options, step);
                latitude[p] = step.getLatitude();
                longitude[p] = step.getLongitude();
                distance[p] = step.getDistance();
                duration[p] = step.getDuration();
                movingTime[p] = step.getMovingTime();
                speed[p] = step.getSpeed();
                elevation[p] = step.getElevation();
                ascent[p] = step.getAscent();
                descend[p] = step.getDescend();
                grade[p] = step.getGrade();
                cadence[p] = step.getCadence();
                heartRate[p] = step.getHeartRate();
                airTemp[p] = step.getTemperature();
                power[p] = step.getPower();
                maxSpeed = Math.max(maxSpeed, speed[p]);
                minElevation = Math.min(minElevation, elevation[p]);
                maxElevation = Math.max(maxElevation, elevation[p]);
                avgCadence += cadence[p];
                maxCadence = Math.max(maxCadence, cadence[p]);
                avgTemp += airTemp[p];
                avgWatts += power[p];
                avgHearRate += heartRate[p];
                minHeartRate = Math.min(minHeartRate, heartRate[p]);
                maxHeartRate = Math.max(maxHeartRate, heartRate[p]);
                w1 = w2;
                p++;
            }

        Activity activity;
        if (options.contains(DataOption.TIME)) {
            java.sql.Date date = DateTimeUtils.toSqlDate(startPoint.getTime());
            java.sql.Time time = DateTimeUtils.toSqlTime(startPoint.getTime());
            activity = new Activity(track.getName(), date, time);
        } else if (null != metadata){
            java.sql.Date date = DateTimeUtils.toSqlDate(metadata.getTime());
            java.sql.Time time = DateTimeUtils.toSqlTime(metadata.getTime());
            activity = new Activity(track.getName(), date, time);
        } else  activity = new Activity(track.getName());

        if (track.getDesc() != null)
            activity.setDescription(track.getDesc());
        else activity.setDescription("");
        activity.getOptions().addAll(options);
        activity.setDistance(step.getDistance());

        ActivityDataPoints data = new ActivityDataPoints(numPoints);
        data.setLatitude(latitude);
        data.setLongitude(longitude);
        data.setDistance(distance);
        p--;
        if (options.contains(DataOption.ELEVATION)) {
            activity.setMinElevation(minElevation);
            activity.setMaxElevation(maxElevation);
            activity.setAscent(ascent[p]);
            activity.setDescend(descend[p]);
            data.setElevation(elevation);
            data.setAscent(ascent);
            data.setDescend(descend);
            data.setGrade(grade);
        }
        if (options.contains(DataOption.TIME)) {
            activity.setDuration(duration[p]/ 1000);
            activity.setMovingTime(movingTime[p] / 1000);
            activity.setAverageSpeed(distance[p] * 3600 / movingTime[p]);
            activity.setMaxSpeed(maxSpeed * 3.6);
            data.setDuration(duration);
            data.setMovingTime(movingTime);
            data.setSpeed(speed);
        }
        if (options.contains(DataOption.CADENCE)) {
            activity.setAverageCadence(avgCadence / numPoints);
            activity.setMaxCadence(maxCadence);
            data.setCadence(cadence);
        }
        if (options.contains(DataOption.HEART_RATE)) {
            activity.setAverageHeartrate(avgHearRate / numPoints);
            activity.setMinHeartrate(minHeartRate);
            activity.setMaxHeartrate(maxHeartRate);
            data.setHeartRate(heartRate);
        }
        if (options.contains(DataOption.AIR_TEMP)) {
            activity.setAverageTemp(avgTemp / numPoints);
            data.setAirTemp(airTemp);
        }
        if (options.contains(DataOption.POWER)) {
            activity.setAverageWatts(avgWatts / numPoints);
            data.setPower(power);
        }
        activity.setData(data);
        return activity;
    }

    static public ActivityStep convert(WptType w1, WptType w2, Set<DataOption> options, ActivityStep previousStep) {
        double meters = DISTANCE_FUNCTION.getDistance(w1.getLat().doubleValue(), w1.getLon().doubleValue(),
                w2.getLat().doubleValue(), w2.getLon().doubleValue());
        long t1 = w1.getTime().toGregorianCalendar().getTimeInMillis();
        long t2 = w2.getTime().toGregorianCalendar().getTimeInMillis();
        int milliseconds = (int) (t2 - t1);
        ActivityStep step = new ActivityStep(w2.getLat().doubleValue(), w2.getLon().doubleValue(), previousStep.getDistance() + meters);
        step.setDuration(previousStep.getDuration() + milliseconds);
        double speed;
        if (milliseconds > 100)
            speed = meters * 1000.0 / milliseconds;
        else speed = 0;
        step.setSpeed(speed);
        if (speed > SPEED_THRESHOLD)
            step.setMovingTime(previousStep.getMovingTime() + milliseconds);
        else step.setMovingTime(previousStep.getMovingTime());

        if (options.contains(DataOption.ELEVATION)) {
            step.setElevation(w2.getEle().doubleValue());
            double ramp = w2.getEle().doubleValue() - w1.getEle().doubleValue();
            step.setAscent(previousStep.getAscent() + Math.max(0, ramp));
            step.setDescend(previousStep.getDescend() - Math.min(0, ramp));
        }

        if (null != w2.getExtensions())
            for (Object o : w2.getExtensions().getAny()) {
                JAXBElement element = (JAXBElement) o;
                Object extension = element.getValue();
                switch (element.getName().getLocalPart()) {
                    case "TrackPointExtension":
                        TrackPointExtensionT trackPointExtension = (TrackPointExtensionT) extension;
                        if (null != trackPointExtension.getHr()) step.setHeartRate(trackPointExtension.getHr());
                        if (null != trackPointExtension.getCad()) step.setCadence(trackPointExtension.getCad());
                        if (null != trackPointExtension.getAtemp()) step.setTemperature(trackPointExtension.getAtemp());
                        // TODO add support for aquatic activities
//                        if (null != trackPointExtension.getWtemp()) step.setWaterTemp(trackPointExtension.getWtemp());
//                        if (null != trackPointExtension.getDepth()) step.setDepth(trackPointExtension.getDepth());
                        break;
                    case "PowerInWatts":
                        Short power = (Short) extension;
                        step.setPower(power);
                }
            }
        return step;
    }

    static private Set<DataOption> findOutDataAvailable(WptType w) {
        Set<DataOption> options = EnumSet.noneOf(DataOption.class);
        if (w.getLat() != null) options.add(DataOption.GEOLOCATION);
        if (w.getEle() != null) options.add(DataOption.ELEVATION);
        if (w.getTime() != null) options.add(DataOption.TIME);
        if (null != w.getExtensions())
            for (Object o : w.getExtensions().getAny()) {
                JAXBElement element = (JAXBElement) o;
                Object extension = element.getValue();
                switch (element.getName().getLocalPart()) {
                    case "TrackPointExtension":
                        TrackPointExtensionT tpx = (TrackPointExtensionT) extension;
                        if (null != tpx.getHr()) options.add(DataOption.HEART_RATE);
                        if (null != tpx.getCad()) options.add(DataOption.CADENCE);
                        if (null != tpx.getAtemp()) options.add(DataOption.AIR_TEMP);
                        if (null != tpx.getWtemp()) options.add(DataOption.WATER_TEMP);
                        if (null != tpx.getDepth()) options.add(DataOption.DEPTH);
                        break;
                    case "PowerInWatts":
                        options.add(DataOption.POWER);
                }
            }

        return options;
    }

}
