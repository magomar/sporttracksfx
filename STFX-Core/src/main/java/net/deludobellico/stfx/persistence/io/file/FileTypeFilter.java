package net.deludobellico.stfx.persistence.io.file;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.nio.file.FileSystems;
import java.nio.file.Path;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class FileTypeFilter implements FileFilter, FilenameFilter {

    private final String description;
    private final String[] extensions;

    public FileTypeFilter(String description, String extension) {
        this(description, new String[]{extension});
    }

    public FileTypeFilter(String description, String extensions[]) {
        if (description == null) {
            this.description = extensions[0];
        } else {
            this.description = description;
        }
        this.extensions = extensions.clone();
        for (int i = 0, n = this.extensions.length; i < n; i++) {
            this.extensions[i] = this.extensions[i].toLowerCase();
        }
    }

    public FileTypeFilter(FileType fileType) {
        this(fileType.getDescription(), fileType.getFileExtension());
    }

    @Override
    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        } else {
            String path = file.getAbsolutePath().toLowerCase();
            for (int i = 0, n = extensions.length; i < n; i++) {
                String extension = extensions[i];
                if (path.endsWith(extension)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean accept(File dir, String name) {
        Path path = FileSystems.getDefault().getPath(dir.getPath().toString(), name);
        return accept(path.toFile());
    }

    public String getDescription() {
        return description;
    }

    public String[] getExtensions() {
        return extensions;
    }
}
