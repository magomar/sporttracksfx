package net.deludobellico.stfx.persistence.service;

import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.model.UserProfile;
import net.deludobellico.stfx.persistence.dao.Dao;
import net.deludobellico.stfx.persistence.dao.DaoException;

import javax.inject.Inject;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 22/07/2016.
 *         see https://gist.github.com/james-d/ab72b487b7b12bb7a3bc
 */
public class ServiceImpl implements StfxService {
    private Dao dao;

    public Dao getDao() {
        return dao;
    }

    @Inject
    public void setDao(Dao dao) {
        this.dao = dao;
    }

    @Override
    public List<UserProfile> getAllUserProfiles() throws ServiceException {
        try {
            return dao.getAllUserProfiles();
        } catch (DaoException exc) {
            throw new ServiceException(exc);
        }
    }

    @Override
    public void addUserProfile(UserProfile userProfiles) throws ServiceException {
        try {
            dao.addUserProfile(userProfiles);
        } catch (DaoException exc) {
            throw new ServiceException(exc);
        }
    }

    @Override
    public void updateUserProfile(UserProfile userProfile) throws ServiceException {
        try {
            dao.updateUserProfile(userProfile);
        } catch (DaoException exc) {
            throw new ServiceException(exc);
        }
    }

    @Override
    public void removeUserProfile(UserProfile userProfile) throws ServiceException {
        try {
            dao.removeUserProfile(userProfile);
        } catch (DaoException exc) {
            throw new ServiceException(exc);
        }
    }

//    @Override
//    @Transactional
//    public List<Activity> getActivitiesByUserProfile(UserProfile userProfile) throws ServiceException {
//        try {
//            return dao.getActivitiesByUserProfile(userProfile);
//        } catch (DaoException exc) {
//            throw new ServiceException(exc);
//        }
//    }


    @Override
    public void addActivity(Activity activity, UserProfile userProfile) throws ServiceException {
        try {
            dao.addActivity(activity, userProfile);
        } catch (DaoException exc) {
            throw new ServiceException(exc);
        }
    }

    @Override
    public void addActivities(List<Activity> activity, UserProfile userProfile) throws ServiceException {
        try {
            dao.addActivities(activity, userProfile);
        } catch (DaoException exc) {
            throw new ServiceException(exc);
        }
    }

    @Override
    public void updateActivity(Activity activity) throws ServiceException {
        try {
            dao.updateActivity(activity);
        } catch (DaoException exc) {
            throw new ServiceException(exc);
        }
    }

    @Override
    public void removeActivity(Activity activity, UserProfile userProfile) throws ServiceException {
        try {
            dao.removeActivity(activity, userProfile);
        } catch (DaoException exc) {
            throw new ServiceException(exc);
        }
    }
}
