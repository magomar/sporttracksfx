package net.deludobellico.stfx.persistence.service;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 * Created on 22/07/2016.
 */
public class ServiceException extends RuntimeException {

    public ServiceException() {
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
