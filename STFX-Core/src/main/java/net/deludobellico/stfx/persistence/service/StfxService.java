package net.deludobellico.stfx.persistence.service;

import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.model.UserProfile;

import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 22/07/2016.
 */
public interface StfxService {
    
    // ---------------- UserProfile ----------------

    List<UserProfile> getAllUserProfiles() throws ServiceException;

//    UserProfile getUserProfileById(int id) throws ServiceException;

    void addUserProfile(UserProfile userProfile) throws ServiceException;

    void updateUserProfile(UserProfile userProfile) throws ServiceException;

    void removeUserProfile(UserProfile userProfile) throws ServiceException;

    // ---------------- Activity ----------------

//    List<Activity> getActivitiesByUserProfile(UserProfile userProfile) throws ServiceException;

//    Activity getActivityById(int id) throws ServiceException;

    void addActivity(Activity activity, UserProfile userProfile) throws ServiceException;

    void addActivities(List<Activity> activity, UserProfile userProfile) throws ServiceException;

    void updateActivity(Activity activity) throws ServiceException;

    void removeActivity(Activity activity, UserProfile userProfile) throws ServiceException;
}
