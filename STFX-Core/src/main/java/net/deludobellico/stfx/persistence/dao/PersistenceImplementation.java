package net.deludobellico.stfx.persistence.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 22/07/2016.
 */
@Deprecated
public enum PersistenceImplementation {
    OBJECTDB("$objectdb/db");
    private final String persistenceUnitPrefix;

    PersistenceImplementation(String persistenceUnitPrefix) {
        this.persistenceUnitPrefix = persistenceUnitPrefix;
    }

    public EntityManager getEntityManager(String dbName) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(String.format(persistenceUnitPrefix + "/%s", dbName));
        return emf.createEntityManager();
    }

}
