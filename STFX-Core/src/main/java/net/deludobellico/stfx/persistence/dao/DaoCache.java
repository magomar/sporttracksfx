package net.deludobellico.stfx.persistence.dao;

import net.deludobellico.stfx.persistence.PersistenceConfig;
import org.aeonbits.owner.ConfigCache;

import javax.persistence.Persistence;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class DaoCache {
    private static final ConcurrentMap<PersistenceFramework, Dao> CACHE = new ConcurrentHashMap<>();

    /**
     * Don't let anyone instantiate this class
     */
    private DaoCache() {
    }


    /**
     * Gets from the cache or create, an instance of the given class using the given imports.
     *
     * @return an object implementing the Dao interface, that can be taken from the cache.
     */
    public static Dao getOrCreate() {
        return getOrCreate(PersistenceFramework.JPA);
    }


    /**
     * Gets from the cache or create, an instance of the given Dao type using the given imports.
     *
     * @param persistenceFramework the type of Data Access Layer
     * @return an object implementing the Dao interface, that can be taken from the cache.
     */
    public static Dao getOrCreate(PersistenceFramework persistenceFramework) {
        Dao existing = get(persistenceFramework);
        if (existing != null) return existing;
        Dao created = DaoFactory.create(persistenceFramework);
        Dao raced = add(persistenceFramework, created);
        return raced != null ? raced : created;
    }

    /**
     * Gets from the cache the {@link Dao} instance identified by the given key.
     *
     * @param key the key object to be used to identify the instance in the cache.
     * @return the {@link Dao} object from the cache if exists, or <tt>null</tt> if it doesn't.
     */
    public static Dao get(PersistenceFramework key) {
        return CACHE.get(key);
    }

    /**
     * Adds a {@link Dao} object into the cache.
     *
     * @param key      the key object to be used to identify the instance in the cache.
     * @param instance the instance of the {@link Dao} object to be stored into the cache.
     * @return the previous value associated with the specified key, or
     * <tt>null</tt> if there was no mapping for the key.
     */
    public static Dao add(PersistenceFramework key, Dao instance) {
        return CACHE.putIfAbsent(key, instance);
    }

    /**
     * Lists the key objects for all Dao instances present in the cache.
     *
     * @return a set containing the key objects for all instance in the cache.
     */
    public static Set<Object> list() {
        // Return an unmodifiableSet to ensure that the caller does not modify the contents of our
        // private map via the result of this call. The key objects themselves are the same as
        // those contained in the private map, which means that if they are mutable, the caller
        // will be able to affect the contents of the map (albeit only the keys).
        return Collections.unmodifiableSet(CACHE.keySet());
    }

    /**
     * Removes all of the cached instances.
     * The cache will be empty after this call returns.
     */
    public static void clear() {
        CACHE.clear();
    }

    /**
     * Removes the cached instance for the given key if it is present.
     * <p>
     * <p>Returns previous instance associated to the given key in the cache,
     * or <tt>null</tt> if the cache contained no instance for the given key.
     * <p>
     * <p>The cache will not contain the instance for the specified key once the
     * call returns.
     *
     * @param key key whose instance is to be removed from the cache.
     * @return the previous instance associated with <tt>key</tt>, or
     * <tt>null</tt> if there was no instance for <tt>key</tt>.
     */
    public static Dao remove(PersistenceFramework key) {
        return CACHE.remove(key);
    }

}
