package net.deludobellico.stfx.persistence.dao;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 * Created on 22/07/2016.
 */
public class DaoException extends Exception {

    public DaoException() {
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public DaoException(String message) {
        super(message);
    }

    public DaoException(Throwable cause) {
        super(cause);
    }

}

