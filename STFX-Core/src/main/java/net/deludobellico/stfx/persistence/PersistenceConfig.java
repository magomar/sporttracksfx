package net.deludobellico.stfx.persistence;


import net.deludobellico.stfx.persistence.dao.Dao;
import net.deludobellico.stfx.persistence.dao.PersistenceFramework;
import org.aeonbits.owner.Config;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public interface PersistenceConfig extends Config {

    @DefaultValue("net.deludobellico.stfx.persistence.dao.jpa.JpaDao")
    @Key("persistence.framework")
    String persistenceFramework();

    @DefaultValue("STFX")
    @Key("persistence.unitName")
    String persistenceUnitName();
}
