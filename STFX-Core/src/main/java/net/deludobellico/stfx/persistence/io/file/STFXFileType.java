package net.deludobellico.stfx.persistence.io.file;

import net.deludobellico.stfx.persistence.io.IOConfig;
import net.deludobellico.stfx.persistence.jaxb.JAXBTools;
import net.deludobellico.stfx.persistence.jaxb.gpx.GpxType;
import net.deludobellico.stfx.persistence.jaxb.tcx.TrainingCenterDatabaseT;
import org.aeonbits.owner.ConfigCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public enum STFXFileType implements FileType {
    GPX(new String[]{".gpx"}, "GPS eXchange Format",
            GpxType.class,
            "xsd/gpx-v1.1-extended.xsd",
            ConfigCache.getOrCreate(IOConfig.class).jaxbGpxUrl(),
            GpxFileDriver.class),
    TCX(new String[]{".tcx"}, "Training Center XML",
            TrainingCenterDatabaseT.class,
            "xsd/TrainingCenterDatabasev2.xsd",
            ConfigCache.getOrCreate(IOConfig.class).jaxbTcxUrl(),
            TcxFileDriver.class);

    private static final Logger LOGGER = LoggerFactory.getLogger(STFXFileType.class);
    private final String[] fileExtension;
    private final String description;
    private final Class xmlRootClass;
    private final FileTypeFilter fileTypeFilter;
    private final String schemaUrl;
    private final String schemaFile;
    private final Class<? extends FileDriver> fileDriverClass;

    private FileDriver fileDriver;
    private JAXBTools jaxbTools;
    private Validator validator;

    STFXFileType(final String[] fileExtension, final String description, final Class xmlRootClass, String schemaFile,
                 String schemaUrl, Class<? extends FileDriver> fileDriverClass) {
        this.fileExtension = fileExtension;
        this.description = description;
        this.schemaUrl = schemaUrl;
        this.schemaFile = schemaFile;
        this.fileTypeFilter = new FileTypeFilter(this);
        this.xmlRootClass = xmlRootClass;
        this.fileDriverClass = fileDriverClass;
    }

    @Override
    public String[] getFileExtension() {
        return fileExtension;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Class getXmlRootClass() {
        return xmlRootClass;
    }

    @Override
    public FileTypeFilter getFileTypeFilter() {
        return fileTypeFilter;
    }

    @Override
    public FileDriver getFileDriver() {
        if (null != fileDriver) return fileDriver;
        Constructor<? extends FileDriver> constructor = null;
        try {
            constructor = fileDriverClass.getConstructor(FileType.class);
            fileDriver = constructor.newInstance(new Object[]{this});
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            LOGGER.error("Error creating new instance of {}", fileDriverClass);
        }
        return fileDriver;
    }


    @Override
    public JAXBTools getJaxbTools() {
        if (null != jaxbTools) return jaxbTools;
        try {
            jaxbTools = new JAXBTools(xmlRootClass);
        } catch (JAXBException e) {
            LOGGER.error("Error creating JAXBTools with context class {}", xmlRootClass);
        }
        return jaxbTools;
    }

    @Override
    public Validator getValidator() {
        if (null != validator) return validator;
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = null;
        try {
            schema = schemaFactory.newSchema(STFXFileType.class.getClassLoader().getResource(schemaFile));
        } catch (SAXException e) {
            URL url = null;
            try {
                url = new URL(schemaUrl);
                schema = schemaFactory.newSchema(new StreamSource(url.openStream()));
            } catch (SAXException | IOException e1) {
                LOGGER.error("Error creating XML validation schema for {}", this);
            }
        }
        if (null != schema) validator = schema.newValidator();
        return validator;
    }


    @Override
    public FileType fromFileExtension(String extension) {
        if (extension != null) {
            for (STFXFileType fileType : STFXFileType.values())
                for (String ext : fileType.getFileExtension())
                    if (extension.equalsIgnoreCase(ext))
                        return fileType;
        }
        return null;
    }
}

