package net.deludobellico.stfx.persistence.dao;

import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.model.UserProfile;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */

public interface Dao {

    // ---------------- UserProfile ----------------

    List<UserProfile> getAllUserProfiles() throws DaoException;

//    UserProfile getUserProfileById(int id) throws DaoException;

    @Inject
    void setEntityManager(EntityManager em);

    void addUserProfile(UserProfile userProfile) throws DaoException;

    void updateUserProfile(UserProfile userProfile) throws DaoException;

    void removeUserProfile(UserProfile userProfile) throws DaoException;

    // ---------------- Activity ----------------

//    List<Activity> getActivitiesByUserProfile(UserProfile userProfile) throws DaoException;

//    Activity getActivityById(int id) throws DaoException;

    void addActivity(Activity activity, UserProfile userProfile) throws DaoException;

    void addActivities(List<Activity> activity, UserProfile userProfile) throws DaoException;

    void updateActivity(Activity activity) throws DaoException;

    void removeActivity(Activity activity, UserProfile userProfile) throws DaoException;


}
