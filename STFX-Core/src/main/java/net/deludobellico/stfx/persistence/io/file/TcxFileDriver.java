package net.deludobellico.stfx.persistence.io.file;

import net.deludobellico.stfx.model.Activity;

import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class TcxFileDriver implements FileDriver {
    @Override
    public List<Activity> importActivities(File directory, FilenameFilter filter, boolean recursively) {
        return null;
    }

    @Override
    public List<Activity> importActivities(File file) {
        return null;
    }

    @Override
    public List<Activity> importActivities(File file, boolean validate) {
        return null;
    }

    @Override
    public List<Activity> importActivities(InputStream inputStream, boolean validate) {
        return null;
    }

    @Override
    public List<Activity> importActivities(InputStream is) {
        return null;
    }
}
