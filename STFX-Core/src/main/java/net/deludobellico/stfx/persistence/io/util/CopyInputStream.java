package net.deludobellico.stfx.persistence.io.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CopyInputStream {
    private InputStream inputStream;
    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    /**
     *
     */
    public CopyInputStream(InputStream is) {
        this.inputStream = is;
        try {
            copy();
        } catch (IOException ex) {
            // do nothing
        }
    }

    private int copy() throws IOException {
        int read = 0;
        int chunk;
        byte[] data = new byte[256];
        while (-1 != (chunk = inputStream.read(data))) {
            read += data.length;
            outputStream.write(data, 0, chunk);
        }
        return read;
    }

    public InputStream getCopy() {
        return new ByteArrayInputStream(outputStream.toByteArray());
    }
}