package net.deludobellico.stfx.persistence.jaxb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.*;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;


public class JAXBTools {
    private static final Logger LOGGER = LoggerFactory.getLogger(JAXBTools.class);
    private final JAXBContext jaxbContext;
    private final Marshaller marshaller;
    private final Unmarshaller unmarshaller;

    public JAXBTools(Class contextClass) throws JAXBException {
        jaxbContext = JAXBContext.newInstance(contextClass);
        unmarshaller = jaxbContext.createUnmarshaller();
        marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    }

    public JAXBTools(Class... contextClass) throws JAXBException {
        jaxbContext = JAXBContext.newInstance(contextClass);
        unmarshaller = jaxbContext.createUnmarshaller();
        marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    }

    public JAXBTools(String contextPath) throws JAXBException {
        jaxbContext = JAXBContext.newInstance(contextPath);
        unmarshaller = jaxbContext.createUnmarshaller();
        marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    }

    /**
     * Unmarshalls XML element from file into java object
     *
     * @param file the XML file to be unmarshalled
     * @return the object unmarshalled from the {@code file}
     */
    public Object unmarshallFile(File file) {
        Object object = null;
        try {
            StreamSource source = new StreamSource(file);
            JAXBElement<Object> root = (JAXBElement<Object>) unmarshaller.unmarshal(source);
            object = root.getValue();
        } catch (JAXBException ex) {
            LOGGER.error("JAXB exception unmarshalling from {}", file);
            LOGGER.error(ex.getMessage());
        }
        return object;
    }

    /**
     * Unmarshalls XML element from InputStream into java object
     *
     * @param inputStream the input stream to be unmarshalled
     * @return the object unmarshalled from the {@code inputStream}
     */
    public Object unmarshallStream(InputStream inputStream) {
        Object object = null;
        try {
            JAXBElement<Object> root = (JAXBElement<Object>) unmarshaller.unmarshal(inputStream);
            object = root.getValue();
        } catch (JAXBException ex) {
            LOGGER.error("JAXB exception unmarshalling from input stream");
            LOGGER.error(ex.getMessage());
        }
        return object;
    }


    public Object unmarshallZipped(File file) {
        Object object = null;
        try (ZipFile zipFile = new ZipFile(file.getPath())) {
            ZipEntry zipEntry = zipFile.entries().nextElement();
            InputStream inputStream = zipFile.getInputStream(zipEntry);
            JAXBElement<Object> root = (JAXBElement<Object>) unmarshaller.unmarshal(new StreamSource(inputStream));
            object = root.getValue();
        } catch (JAXBException ex) {
            LOGGER.error("JAXB exception unmarshalling from {}", file);
            LOGGER.error(ex.getMessage());
        } finally {
            return object;
        }
    }

    /**
     * Marshalls Java object into XML file
     *
     * @param object object to be marshalled
     * @param file   file to save the marshalled object
     * @return the XML file
     */
    public File marshallXML(Object object, File file) {
        try {
            try (FileOutputStream fos = new FileOutputStream(file)) {
                marshaller.marshal(object, fos);
                return file;
            } catch (IOException ex) {
                LOGGER.error("IO exception marshalling to {}", file);
                LOGGER.error(ex.getMessage());
            }
        } catch (JAXBException ex) {
            LOGGER.error("JAXB exception marshalling to {}", file);
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    public StringWriter marshallXML(Object object) {
        try {
            try (StringWriter sw = new StringWriter()) {
                marshaller.marshal(object, sw);
                return sw;
            } catch (IOException ex) {
                LOGGER.error("IO exception marshalling to String Writer");
                LOGGER.error(ex.getMessage());
            }
        } catch (JAXBException ex) {
            LOGGER.error("JAXB exception marshalling to String Writer");
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    /**
     * Marshalls Java object in a zipped XMl file
     *
     * @param object object to be marshalled
     * @param file   non zip file to save the marshalled object
     * @return the ZIP file
     */
    public File marshallZipped(Object object, File file) {
        File zipFile = new File(file.getAbsolutePath() + ".zip");
        try {
            FileOutputStream fos = new FileOutputStream(zipFile);
            try (ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fos))) {
                ZipEntry ze = new ZipEntry(file.getName());
                zos.putNextEntry(ze);
                marshaller.marshal(object, zos);
                return zipFile;
            } catch (IOException ex) {
                LOGGER.error("IO exception marshalling and zipping to {}", zipFile);
                LOGGER.error(ex.getMessage());
            }
        } catch (FileNotFoundException ex) {
            LOGGER.error("File not found  marshalling and zipping to {}", zipFile);
            LOGGER.error(ex.getMessage());
        } catch (JAXBException ex) {
            LOGGER.error("Exception marshalling and zipping to {}", zipFile);
            LOGGER.error(ex.getMessage());
        }

        return null;
    }

    /**
     * Marshalls Java object in a gzipped XMl file
     *
     * @param object object to be marshalled
     * @param file   file to save the marshalled object
     * @return the Gzip file
     */
    public File marshallGzipped(Object object, File file) {
        File gzFile = new File(file.getAbsolutePath() + ".gz");
        try {
            FileOutputStream fos = new FileOutputStream(gzFile);
            try {
                GZIPOutputStream gz = new GZIPOutputStream(fos);
                marshaller.marshal(object, gz);
                return gzFile;
            } catch (IOException ex) {
                LOGGER.error("IO exception marshalling and gzipping to {}", gzFile);
                LOGGER.error(ex.getMessage());
            }
        } catch (FileNotFoundException ex) {
            LOGGER.error("File not found  marshalling and gzipping to {}", gzFile);
            LOGGER.error(ex.getMessage());
        } catch (JAXBException ex) {
            LOGGER.error("Exception marshalling andg zipping to {}", gzFile);
            LOGGER.error(ex.getMessage());
        }

        return null;
    }


//    /**
//     * Unmarshalls Json element of type {@code c} from the {@code file}.
//     *
//     * @param c    the class of object to be unmarshalled
//     * @param file the Json file containing the marshalled object
//     * @return the object of type {@code T} from the {@code file}
//     */
//    public static <T> T unmarshallJson(File file, Class<T> c) {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
//        T object = null;
//        try {
//            object = mapper.readValue(file, c);
//        } catch (IOException ex) {
//            LOGGER.log(Level.SEVERE, "Exception unmarshalling Json", ex);
//        }
//        return object;
//    }
//
//    /**
//     * Marshalls Java object into a Json file
//     *
//     * @param object object to be marshalled
//     * @param file   file to save the marshalled object
//     * @return
//     */
//    public static File marshallJson(Object object, File file) {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
//        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
//        try {
//            writer.writeValue(file, object);
//            return file;
//        } catch (IOException ex) {
//            LOGGER.log(Level.SEVERE, "Exception marshalling Json", ex);
//        }
//        return null;
//    }

}
