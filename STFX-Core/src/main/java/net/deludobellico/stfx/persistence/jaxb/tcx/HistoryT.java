
package net.deludobellico.stfx.persistence.jaxb.tcx;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para History_t complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="History_t">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Running" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}HistoryFolder_t"/>
 *         &lt;element name="Biking" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}HistoryFolder_t"/>
 *         &lt;element name="Other" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}HistoryFolder_t"/>
 *         &lt;element name="MultiSport" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}MultiSportFolder_t"/>
 *         &lt;element name="Extensions" type="{http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2}Extensions_t" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "History_t", namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", propOrder = {
    "running",
    "biking",
    "other",
    "multiSport",
    "extensions"
})
public class HistoryT {

    @XmlElement(name = "Running", namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", required = true)
    protected HistoryFolderT running;
    @XmlElement(name = "Biking", namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", required = true)
    protected HistoryFolderT biking;
    @XmlElement(name = "Other", namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", required = true)
    protected HistoryFolderT other;
    @XmlElement(name = "MultiSport", namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", required = true)
    protected MultiSportFolderT multiSport;
    @XmlElement(name = "Extensions", namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")
    protected ExtensionsT extensions;

    /**
     * Obtiene el valor de la propiedad running.
     * 
     * @return
     *     possible object is
     *     {@link HistoryFolderT }
     *     
     */
    public HistoryFolderT getRunning() {
        return running;
    }

    /**
     * Define el valor de la propiedad running.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoryFolderT }
     *     
     */
    public void setRunning(HistoryFolderT value) {
        this.running = value;
    }

    /**
     * Obtiene el valor de la propiedad biking.
     * 
     * @return
     *     possible object is
     *     {@link HistoryFolderT }
     *     
     */
    public HistoryFolderT getBiking() {
        return biking;
    }

    /**
     * Define el valor de la propiedad biking.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoryFolderT }
     *     
     */
    public void setBiking(HistoryFolderT value) {
        this.biking = value;
    }

    /**
     * Obtiene el valor de la propiedad other.
     * 
     * @return
     *     possible object is
     *     {@link HistoryFolderT }
     *     
     */
    public HistoryFolderT getOther() {
        return other;
    }

    /**
     * Define el valor de la propiedad other.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoryFolderT }
     *     
     */
    public void setOther(HistoryFolderT value) {
        this.other = value;
    }

    /**
     * Obtiene el valor de la propiedad multiSport.
     * 
     * @return
     *     possible object is
     *     {@link MultiSportFolderT }
     *     
     */
    public MultiSportFolderT getMultiSport() {
        return multiSport;
    }

    /**
     * Define el valor de la propiedad multiSport.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiSportFolderT }
     *     
     */
    public void setMultiSport(MultiSportFolderT value) {
        this.multiSport = value;
    }

    /**
     * Obtiene el valor de la propiedad extensions.
     * 
     * @return
     *     possible object is
     *     {@link ExtensionsT }
     *     
     */
    public ExtensionsT getExtensions() {
        return extensions;
    }

    /**
     * Define el valor de la propiedad extensions.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtensionsT }
     *     
     */
    public void setExtensions(ExtensionsT value) {
        this.extensions = value;
    }

}
