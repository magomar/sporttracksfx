package net.deludobellico.stfx.persistence.io.file;

import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.persistence.io.IOConfig;
import net.deludobellico.stfx.persistence.io.util.CopyInputStream;
import net.deludobellico.stfx.persistence.io.util.FileIO;
import net.deludobellico.stfx.persistence.jaxb.gpx.GpxType;
import org.aeonbits.owner.ConfigCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class GpxFileDriver implements FileDriver {
    private static final Logger LOGGER = LoggerFactory.getLogger(GpxFileDriver.class);
    private final FileType fileType;

    public GpxFileDriver(FileType fileType) {
        this.fileType = fileType;
    }

    @Override
    public List<Activity> importActivities(File directory, FilenameFilter filter, boolean recursively) {
        List<File> files = FileIO.listFiles(directory, filter, recursively);
        List<Activity> activities = new ArrayList<>(files.size());
        for (File file : files) {
            activities.addAll(importActivities(file));
        }
        return activities;
    }

    @Override
    public List<Activity> importActivities(File file) {
        IOConfig config = ConfigCache.getOrCreate(IOConfig.class);
        boolean validate = config.jaxbValidate();
        return importActivities(file, validate);
    }

    @Override
    public List<Activity> importActivities(File file, boolean validate) {
        try {
            InputStream is = new FileInputStream(file);
            return importActivities(is, validate);
        } catch (FileNotFoundException e) {
            LOGGER.error("File {} not found", file);
        }
        return null;
    }

    @Override
    public List<Activity> importActivities(InputStream is) {
        IOConfig config = ConfigCache.getOrCreate(IOConfig.class);
        return importActivities(is, config.jaxbValidate());
    }

    @Override
    public List<Activity> importActivities(InputStream inputStream, boolean validate) {
        if (validate) {
            return validate(inputStream);
        }
        GpxType gpx = (GpxType) fileType.getJaxbTools().unmarshallStream(inputStream);
        if (null == gpx) {
            return null;
        }
        return GpxConverter.convert(gpx);
    }

    private List<Activity> validate(InputStream input) {
        CopyInputStream cis = new CopyInputStream(input);
        InputStream input1 = cis.getCopy();
        InputStream input2 = cis.getCopy();
        GpxType gpx = null;
        try {
            fileType.getValidator().validate(new StreamSource(input1));
            gpx = (GpxType) fileType.getJaxbTools().unmarshallStream(input2);
        } catch (SAXException | IOException e) {
            LOGGER.error("Error validating {}", fileType);
            LOGGER.error(e.getMessage());
        }
        if (null == gpx) return null;
        return GpxConverter.convert(gpx);
    }
}
