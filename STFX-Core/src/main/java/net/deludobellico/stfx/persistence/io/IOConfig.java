package net.deludobellico.stfx.persistence.io;

import org.aeonbits.owner.Config;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public interface IOConfig extends Config {
    @Config.DefaultValue("false")
    @Config.Key("persistence.jaxb.validate")
    boolean jaxbValidate();

    @Config.DefaultValue("SportTracksFX")
    @Config.Key("persistence.jaxb.appName")
    String jaxbAppName();

//    @Config.DefaultValue("/xsd/gpx-1.1-extended.xsd")
//    @Config.Key("persistence.jaxb.gpx.schema")
//    String jaxbGpxSchema();

    @Config.DefaultValue("http://www.topografix.com/GPX/1/1/gpx.xsd")
    @Config.Key("persistence.jaxb.gpx.url")
    String jaxbGpxUrl();

    @Config.DefaultValue("1.1")
    @Config.Key("persistence.jaxb.gpx.version")
    String jaxbGpxVersion();

//    @Config.DefaultValue("/xsd/TrainingCenterDatabasev2.xsd")
//    @Config.Key("persistence.jaxb.tcx.schema")
//    String jaxbTcxSchema();

    @Config.DefaultValue("http://www8.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd")
    @Config.Key("persistence.jaxb.tcx.url")
    String jaxbTcxUrl();

    @Config.DefaultValue("v2")
    @Config.Key("persistence.jaxb.tcx.version")
    String jaxbTcxVersion();

    @Config.DefaultValue("UTF-8")
    @Config.Key("persistence.jaxb.encoding")
    String jaxbEncoding();

    @Config.DefaultValue("true")
    @Config.Key("persistence.jaxb.newLine")
    boolean jaxbNewLine();

    @Config.DefaultValue("true")
    @Config.Key("persistence.jaxb.formattedOutput")
    boolean jaxbFormattedOutput();

    @DefaultValue("HAVERSINE")
    @Key("analysis.distanceFunction")
    String distanceFunctionName();

    @DefaultValue("0.1")
    @Key("analysis.speedThreshold")
    double speedThreshold();

    @DefaultValue("0.1")
    @Key("analysis.distanceThreshold")
    double distanceThreshold();

    @DefaultValue("30.0")
    @Key("analysis.heartRateThreshold")
    double heartRateThreshold();
}
