package net.deludobellico.stfx.persistence.dao;

import net.deludobellico.stfx.persistence.PersistenceConfig;
import org.aeonbits.owner.ConfigCache;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public final class DaoFactory {

    public static Dao create() {
        return create(PersistenceFramework.JPA);
    }

    public static Dao create(PersistenceFramework persistenceFramework) {
        try {
            Class<? extends Dao> clazz = persistenceFramework.getDaoClass();
            Constructor<? extends Dao> constructor = clazz.getConstructor();
            return constructor.newInstance();
        } catch (ClassNotFoundException | InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            LoggerFactory.getLogger(DaoFactory.class).error("Error creating instance of class {}", persistenceFramework);
            LoggerFactory.getLogger(DaoFactory.class).error(e.toString());
        }
        return null;
    }


}
