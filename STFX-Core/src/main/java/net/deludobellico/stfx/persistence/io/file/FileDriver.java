package net.deludobellico.stfx.persistence.io.file;

import net.deludobellico.stfx.model.Activity;

import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public interface FileDriver {

    List<Activity> importActivities(File directory, FilenameFilter filter, boolean recursively);

    List<Activity> importActivities(File file);

    List<Activity> importActivities(File file, boolean validate);

    List<Activity> importActivities(InputStream inputStream, boolean validate);

    List<Activity> importActivities(InputStream is);
}
