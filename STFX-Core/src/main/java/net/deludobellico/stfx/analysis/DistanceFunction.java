package net.deludobellico.stfx.analysis;

/**
 * Characterizes a function used to compute a distance between two geographical points
 */
public interface DistanceFunction {
    /**
     * Gets the distance between two geographical points, specified by latitud and longitud
     *
     * @param latitude1
     * @param longitude1
     * @param latitude2
     * @param longitude2
     * @return
     */
    double getDistance(double latitude1, double longitude1, double latitude2, double longitude2);
    /**
     * Gets the distance between two geographical points, specified by latitud and longitud, plus elevation
     *
     * @param latitude1
     * @param longitude1
     * @param elevation1
     * @param latitude2
     * @param longitude2
     * @param elevation2
     * @return
     */
    double getDistance(double latitude1, double longitude1, double elevation1, double latitude2, double longitude2, double elevation2);
}
