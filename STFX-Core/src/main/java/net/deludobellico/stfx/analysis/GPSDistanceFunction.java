package net.deludobellico.stfx.analysis;

import javafx.geometry.Point2D;

/**
 * Basic implementation of a {@link DistanceFunction} between two geographical points
 * using the <a href="https://en.wikipedia.org/wiki/Haversine_formula">Haversine formula</a>.
 */
public enum GPSDistanceFunction implements DistanceFunction {
    HAVERSINE {
        @Override
        public double getDistance(double lat1, double lon1, double lat2, double lon2) {
            double latDistance = Math.toRadians(lat2 - lat1);
            double lonDistance = Math.toRadians(lon2 - lon1);
            double a = Math.pow(Math.sin(latDistance / 2), 2) +
                    Math.cos(Math.toRadians(lat1))
                            * Math.cos(Math.toRadians(lat2))
                            * Math.pow(Math.sin(lonDistance / 2), 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            return R * c;
        }

        @Override
        public double getDistance(double lat1, double lon1, double elev1, double lat2, double lon2, double elev2) {
            double latDistance = Math.toRadians(lat2 - lat1);
            double lonDistance = Math.toRadians(lon2 - lon1);
            double a = Math.pow(Math.sin(latDistance / 2), 2) +
                    Math.cos(Math.toRadians(lat1))
                            * Math.cos(Math.toRadians(lat2))
                            * Math.pow(Math.sin(lonDistance / 2), 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            double ramp = elev2 - elev1;
            if (ramp == 0) return R * c;
            else return Math.hypot(R * c, ramp);
        }
    },
    SPHERICAL {
        @Override
        public double getDistance(double lat1, double lon1, double lat2, double lon2) {
            double c = Math.acos(Math.sin(lat1) * Math.sin(lat2) +
                    Math.cos(lon1) * Math.cos(lon2) * Math.cos(lon1 - lon2));
            return R * c;
        }
        @Override
        public double getDistance(double lat1, double lon1, double elev1, double lat2, double lon2, double elev2) {
            double c = Math.acos(Math.sin(lat1) * Math.sin(lat2) +
                    Math.cos(lon1) * Math.cos(lon2) * Math.cos(lon1 - lon2));
            double ramp = elev2 - elev1;
            if (ramp == 0) return R * c;
            else return Math.hypot(R * c, ramp);
        }
    };
    /**
     * Earth radius in metres
     */
    final static double R = 6371000;

    /**
     * Gets the middle point between two geographical points.
     *
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return
     */
    public static Point2D getMiddlePoint(double lat1, double lng1, double lat2, double lng2) {
        double lat1r = Math.toRadians(lat1);
        double lng1r = Math.toRadians(lng1);
        double lat2r = Math.toRadians(lat2);
        double lng2r = Math.toRadians(lng2);
        double x1 = Math.cos(lat1r) * Math.cos(lng1r);
        double y1 = Math.cos(lat1r) * Math.sin(lng1r);
        double z1 = Math.sin(lat1r);
        double x2 = Math.cos(lat2r) * Math.cos(lng2r);
        double y2 = Math.cos(lat2r) * Math.sin(lng2r);
        double z2 = Math.sin(lat2r);
        double x = (x1 + x2) / 2;
        double y = (y1 + y2) / 2;
        double z = (z1 + z2) / 2;
        double lon = Math.atan2(y, x);
        double lat = Math.atan2(z, Math.sqrt(x * x + y * y));
        Point2D point2D = new Point2D(Math.toDegrees(lat), Math.toDegrees(lon));
        return point2D;
    }

}
