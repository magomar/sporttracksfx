package net.deludobellico.stfx.analysis;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class ActivityStep {
    private final static DistanceFunction distanceFunction = GPSDistanceFunction.HAVERSINE;
    /**
     * Geographic latitude in degrees. Negative values are south of the equator.
     */
    double latitude;
    /**
     * Geographic longitude in degrees. Negative values are west of the Greenwich meridian.
     */
    double longitude;
    /**
     * Distance travelled in meters from the beginning of the activity
     */
    double distance;
    /**
     * Total time in milliseconds from start
     */
    int duration;
    /**
     * Total moving time in milliseconds from start
     */
    int movingTime;
    /**
     * Speed in meters per second
     */
    double speed;
    /**
     * Altitude in meters (over the sea level)
     */
    double elevation;
    /**
     * ratio between elevation and distance in meters (elevation / distance)
     */
    private double grade;
    /**
     * accumulated increment of altitude
     */
    private double ascent;
    /**
     * accumulated decrement of altitude
     */
    private double descend;
    /**
     * Heart rate in bits per minute
     */
    int heartRate;
    /**
     * Cadence in RPM
     */
    int cadence;
    /**
     * Temperature in Celsius degrees
     */
    double temperature;
    /**
     * Power in wats
     */
    int power;


    public ActivityStep(double latitude, double longitude, double distance) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getMovingTime() {
        return movingTime;
    }

    public void setMovingTime(int movingTime) {
        this.movingTime = movingTime;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getElevation() {
        return elevation;
    }

    public void setElevation(double elevation) {
        this.elevation = elevation;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getCadence() {
        return cadence;
    }

    public void setCadence(int cadence) {
        this.cadence = cadence;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public double getAscent() {
        return ascent;
    }

    public void setAscent(double ascent) {
        this.ascent = ascent;
    }

    public double getDescend() {
        return descend;
    }

    public void setDescend(double descend) {
        this.descend = descend;
    }
}
