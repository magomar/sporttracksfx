package net.deludobellico.stfx.strava;

import javastrava.api.v3.model.StravaActivity;
import javastrava.api.v3.model.StravaMapPoint;
import javastrava.api.v3.model.StravaStream;
import javastrava.api.v3.service.StreamService;
import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.model.ActivityDataPoints;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 23/07/2016.
 */
public class StravaAdapter {
    StreamService streamService;

    public Activity importActivities(StravaActivity stravaActivity) {

        Activity activity = new Activity();
        activity.setAscent(stravaActivity.getTotalElevationGain());
		activity.setAverageCadence(stravaActivity.getAverageCadence().intValue());
        activity.setAverageHeartrate(stravaActivity.getAverageHeartrate());
        activity.setAverageSpeed(stravaActivity.getAverageSpeed());
        activity.setAverageTemp(stravaActivity.getAverageTemp());
        activity.setAverageWatts(stravaActivity.getAverageWatts());
        activity.setCalories(stravaActivity.getCalories());
		activity.setDate(Date.valueOf(stravaActivity.getStartDateLocal().toLocalDate()));
        // TODO compute descend or remove this field from activity, since it is not so interesting
//		activity.setDescend();
        activity.setDescription(stravaActivity.getDescription());
        activity.setDistance(stravaActivity.getDistance());
		activity.setDuration(stravaActivity.getElapsedTime());
        activity.setMaxCadence(activity.getMaxCadence());
        activity.setMaxElevation(stravaActivity.getTotalElevationGain());
		activity.setMovingTime(stravaActivity.getMovingTime());
        activity.setName(stravaActivity.getName());
		activity.setTime(Time.valueOf(stravaActivity.getStartDateLocal().toLocalTime()));
        activity.setWeightedAverageWatts(stravaActivity.getWeightedAverageWatts());

        // TODO
        //		activity.setOptions(activityId);

        return activity;
    }


    public ActivityDataPoints importActivityDataPoints(List<StravaStream> activityStreams){

        ActivityDataPoints activityData = new ActivityDataPoints();

        for (StravaStream stream : activityStreams)
            switch (stream.getType()) {
                case HEARTRATE:
                    activityData.setHeartRate(toIntArray(stream.getData()));
                    break;
                case ALTITUDE:
                    activityData.setElevation(toDoubleArray(stream.getData()));
                    break;
                case CADENCE:
                    activityData.setCadence(toIntArray(stream.getData()));
                    break;
                case DISTANCE:
                    activityData.setDistance(toDoubleArray(stream.getData()));
                    break;
                case MAPPOINT:
                    List<StravaMapPoint> mapPoints = stream.getMapPoints();
                    mapPoints.parallelStream().mapToDouble(p-> p.getLatitude().doubleValue()).toArray();
                    mapPoints.parallelStream().mapToDouble(p-> p.getLongitude().doubleValue()).toArray();
                    break;
                case GRADE:
                    activityData.setGrade(toDoubleArray(stream.getData()));
                    break;
                case MOVING:
                    activityData.setMovingTime(toIntArray(stream.getData()));
                    break;
                case POWER:
                    activityData.setPower(toIntArray(stream.getData()));
                    break;
                case VELOCITY:
                    activityData.setSpeed(toDoubleArray(stream.getData()));
                    break;
                case TEMPERATURE:
                    activityData.setAirTemp(toDoubleArray(stream.getData()));
                    break;
                case TIME:
                    activityData.setDuration(toIntArray(stream.getData()));
                    break;
                case UNKNOWN:
                    break;
            }

        return activityData;
    }

    private int[] toIntArray(List<Float> floats) {
        return floats.parallelStream().mapToInt(Float::intValue).toArray();
    }

    private double[] toDoubleArray(List<Float> floats) {
        return floats.parallelStream().mapToDouble(Float::doubleValue).toArray();
    }


}
