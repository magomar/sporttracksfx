package net.deludobellico.stfx.connections;

import javafx.scene.image.Image;
import net.deludobellico.stfx.model.ExternalApp;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 18/07/2016.
 */
public enum SportApp implements ExternalApp {
    STRAVA("Strava", "strava-logo.png"),
    RIDE_WITH_GPS("RideWithGPS", "ridewithgps-logo.png");

    private final String logoFile;
    private final String description;
    private static Map<SportApp, Image> logos = new EnumMap<>(SportApp.class);

    SportApp(String description, String logoFile) {
        this.description = description;
        this.logoFile = logoFile;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public Image getLogo() {
        Image image = logos.get(this);
        if (null == image) {
            image = new Image(SportApp.class.getResourceAsStream("/images/" + logoFile));
            logos.put(this, image);
        }
        return image;
    }
}
