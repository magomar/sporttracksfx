package net.deludobellico.stfx.connections;

import javastrava.api.v3.model.StravaActivity;
import javastrava.api.v3.service.Strava;
import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.model.UserProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class StravaConverter {

    static public List<Activity> convert(UserProfile profile, Strava strava) {
        List<StravaActivity> activitiesStrava = strava.listAllAuthenticatedAthleteActivities();
        List<Activity> activities = new ArrayList<>(activitiesStrava.size());
        for (StravaActivity stravaActivity : activitiesStrava) {
            activities.add(convert(stravaActivity));
        }
        return activities;
    }

    static public Activity convert(StravaActivity stravaActivity) {

        Activity activity = null;
//        if (options.contains(DataOption.TIME)) {
//            java.sql.Date date = DateTimeUtils.toSqlDate(startPoint.getTime());
//            java.sql.Time time = DateTimeUtils.toSqlTime(startPoint.getTime());
//            activity = new Activity(track.getDescription(), date, time);
//        } else if (null != metadata){
//            java.sql.Date date = DateTimeUtils.toSqlDate(metadata.getTime());
//            java.sql.Time time = DateTimeUtils.toSqlTime(metadata.getTime());
//            activity = new Activity(track.getDescription(), date, time);
//        } else  activity = new Activity(track.getDescription());
//
//        if (track.getDesc() != null)
//            activity.setDescription(track.getDesc());
//        else activity.setDescription("");
//        activity.getOptions().addAll(options);
//        activity.setDistance(step.getDistance());
//
//        ActivityDataPoints data = new ActivityDataPoints(numPoints);
//        data.setLatitude(latitude);
//        data.setLongitude(longitude);
//        data.setDistance(distance);
//        p--;
//        if (options.contains(DataOption.ELEVATION)) {
//            activity.setMinElevation(minElevation);
//            activity.setMaxElevation(maxElevation);
//            activity.setAscent(ascent[p]);
//            activity.setDescend(descend[p]);
//            data.setElevation(elevation);
//            data.setAscent(ascent);
//            data.setDescend(descend);
//            data.setGrade(grade);
//        }
//        if (options.contains(DataOption.TIME)) {
//            activity.setDuration(duration[p]/ 1000);
//            activity.setMovingTime(movingTime[p] / 1000);
//            activity.setAverageSpeed(distance[p] * 3600 / movingTime[p]);
//            activity.setMaxSpeed(maxSpeed * 3.6);
//            data.setDuration(duration);
//            data.setMovingTime(movingTime);
//            data.setSpeed(speed);
//        }
//        if (options.contains(DataOption.CADENCE)) {
//            activity.setAverageCadence(avgCadence / numPoints);
//            activity.setMaxCadence(maxCadence);
//            data.setCadence(cadence);
//        }
//        if (options.contains(DataOption.HEART_RATE)) {
//            activity.setAverageHeartrate(avgHearRate / numPoints);
//            activity.setMinHeartrate(minHeartRate);
//            activity.setMaxHeartrate(maxHeartRate);
//            data.setHeartRate(heartRate);
//        }
//        if (options.contains(DataOption.AIR_TEMP)) {
//            activity.setAverageTemp(avgTemp / numPoints);
//            data.setAirTemp(airTemp);
//        }
//        if (options.contains(DataOption.POWER)) {
//            activity.setAverageWatts(avgWatts / numPoints);
//            data.setPower(power);
//        }
//        activity.setData(data);
        return activity;
    }


}
