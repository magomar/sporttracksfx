package net.deludobellico.stfx.connections;

import javastrava.api.v3.auth.AuthorisationService;
import javastrava.api.v3.auth.impl.retrofit.AuthorisationServiceImpl;
import javastrava.api.v3.auth.model.Token;
import javastrava.api.v3.auth.ref.AuthorisationScope;
import javastrava.api.v3.service.Strava;
import net.deludobellico.stfx.auth.Authentification;
import net.deludobellico.stfx.auth.OAuth2Credentials;

public class StravaConnection {
    private static OAuth2Credentials credentials;
    private static Token token = null;

    private static AuthorisationScope[] scopes = {AuthorisationScope.VIEW_PRIVATE, AuthorisationScope.WRITE};

    public Strava getStrava() {

        Strava strava = null;

        try {
            credentials = OAuth2Credentials.Read();
        } catch (Exception ex) {
            System.out.println("Cannot store credentials");
        }

        if (null == credentials) {
            String code = Authentification.getOAuth2Credentials();
            System.out.println("New code: " + code);
            credentials = new OAuth2Credentials();
            credentials.setClientToken(code);
            credentials.Store();
        } else {
            System.out.println("Retrieved code: " + credentials.getClientToken());
        }

        if (null != credentials.getClientToken()) {
            AuthorisationService service = new AuthorisationServiceImpl();
            token = service.tokenExchange(Integer.valueOf(OAuth2Credentials.clientId),
                    OAuth2Credentials.clientSecret,
                    credentials.getClientToken(), scopes);
        }
        if (null != token) {
            strava = new Strava(token);
        }
        return strava;

    }


}
