package net.deludobellico.stfx.app.controller.secondary;

import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.deludobellico.stfx.app.guice.STFXModule;
import net.deludobellico.stfx.model.UserProfile;
import net.deludobellico.stfx.persistence.dao.DaoFactory;
import net.deludobellico.stfx.persistence.dao.Dao;
import net.deludobellico.stfx.persistence.dao.PersistenceFramework;
import net.deludobellico.stfx.util.SampleObjectFactory;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import util.JavaFXThreadingRule;

import java.util.Locale;
import java.util.ResourceBundle;

import static javafx.application.Platform.isFxApplicationThread;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 21/07/2016.
 */
public class EditProfileControllerTest {
    static EditProfileController controller;

    @Rule
    public JavaFXThreadingRule javafxRule = new JavaFXThreadingRule();

    public static class AsNonApp extends Application {
        @Override
        public void start(Stage stage) throws Exception {
            // Obtain a Guice injector
            Injector injector = Guice.createInjector(new STFXModule());
            FXMLLoader fxmlLoader = injector.getInstance(FXMLLoader.class);
            fxmlLoader.setLocation(getClass().getResource("/fxml/EditProfile.fxml"));
            ResourceBundle bundle = ResourceBundle.getBundle("i18n.messages");
            fxmlLoader.setResources(bundle);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
            controller = fxmlLoader.getController();
            Dao dao = injector.getInstance(Dao.class);
            if (dao.getAllUserProfiles().isEmpty())  {
                dao.addUserProfile(SampleObjectFactory.createUserProfile());
            }
            controller.setUserProfile(dao.getAllUserProfiles().get(0));
        }
    }

    @BeforeClass
    public static void initJFX() throws InterruptedException {
        Thread t = new Thread("JavaFX Init Thread") {
            public void run() {
                Application.launch(AsNonApp.class, new String[0]);
            }
        };
        t.setDaemon(true);
        t.start();
        System.out.printf("FX App thread started\n");
        Thread.sleep(5000);
    }

    @Test
    public void testEditProfileController() throws Exception {
        assert isFxApplicationThread();

    }

}