package net.deludobellico.stfx.app.controller.window;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.deludobellico.stfx.app.controller.MainController;
import net.deludobellico.stfx.app.controller.secondary.AbstractSecondaryController;

import javax.inject.Inject;
import java.io.IOException;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public abstract class AbstractWindowController<T extends WindowController> extends AbstractSecondaryController implements WindowController<T> {


    protected Stage stage;

    @Override
    public Stage getStage() {
        return stage;
    }

    public T init(String viewUrl, Stage owner, MainController mainController) {
        FXMLLoader fxmlLoader = getFXMLLoader(viewUrl, resources);
        try {
            Parent parent = fxmlLoader.load();
            T controller = fxmlLoader.getController();
            controller.getStage().setScene(new Scene(parent));
            controller.getStage().initOwner(owner);
            return controller;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}

