package net.deludobellico.stfx.app.controller.secondary;

import com.jfoenix.controls.JFXPasswordField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import net.deludobellico.stfx.app.AppConfig;
import net.deludobellico.stfx.model.UserProfile;
import org.aeonbits.owner.ConfigCache;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class LoginController extends AbstractSecondaryController {

    @FXML
    private HBox root;

    @FXML
    private ImageView avatar;

    @FXML
    private JFXPasswordField passwordField;

    private UserProfile userProfile;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    public void setUser(UserProfile userProfile) {
        clear();
        this.userProfile = userProfile;
        avatar.setImage(userProfile.getAvatar().getImage());
    }

    @Override
    public void clear() {
        userProfile = null;
        AppConfig appConfig = ConfigCache.getOrCreate(AppConfig.class);
        Image image = new Image(getClass().getResourceAsStream(appConfig.defaultAvatar()));
        avatar.setImage(image);
        passwordField.clear();
        passwordField.requestFocus();

    }

    @FXML
    private void login(ActionEvent event) {
        if (userProfile.getPassword().equals(passwordField.getText())) mainController.showDashboard(userProfile);
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle(resources.getString("dialogs.warn"));
            alert.setHeaderText(resources.getString("dialogs.login.wrongPassword.header"));
            alert.setContentText(resources.getString("dialogs.login.wrongPassword.content"));
            alert.showAndWait();
        }
    }


    @FXML
    private void back(ActionEvent event) {
        mainController.goBack();
    }
}
