package net.deludobellico.stfx.app.model;

import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.persistence.dao.Dao;

/**
 * Created by Mario on 27/07/2016.
 */
public class ActivityWrapper extends AbstractPersistableDataWrapper {
    public ActivityWrapper(Activity activity) {
        super(activity);
    }

    @Override
    public void update() {

    }

    @Override
    public boolean persist(Dao dao) {
        return false;
    }
}
