package net.deludobellico.stfx.app.controller.secondary;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.layout.HBox;
import net.deludobellico.stfx.app.controller.included.AvatarController;
import net.deludobellico.stfx.model.PersistentImage;
import net.deludobellico.stfx.model.UserProfile;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class RegisterUserController extends AbstractSecondaryController {
    @FXML
    private HBox contentPane;
    @FXML
    private JFXPasswordField passwordField;
    @FXML
    private JFXTextField usernameField;
    @FXML
    private JFXButton okButton;
    @FXML
    private JFXButton cancelButton;

    private AvatarController avatarController;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        FXMLLoader fxmlLoader = getFXMLLoader("/fxml/Avatar.fxml", resources);
        try {
            Parent root = fxmlLoader.load();
            avatarController = fxmlLoader.getController();
            avatarController.setMode(AvatarController.AvatarMode.EDIT);
            contentPane.getChildren().add(0, root);
        } catch (IOException e) {
            e.printStackTrace();
        }
        final BooleanBinding invalidUser = Bindings.isEmpty(usernameField.textProperty())
                .or(Bindings.isEmpty(passwordField.textProperty()));
        okButton.disableProperty().bind(invalidUser);
    }

    @FXML
    private void registerUser(ActionEvent event) {
        UserProfile userProfile = new UserProfile(usernameField.getText(), passwordField.getText());
        userProfile.setAvatar(new PersistentImage(avatarController.getAvatar()));
        if (service.getAllUserProfiles().contains(userProfile)) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(resources.getString("dialogs.error"));
            alert.setHeaderText(resources.getString("dialogs.selectFile.header"));
            alert.setContentText(resources.getString("dialogs.selectFile.context"));
            alert.showAndWait();
        } else {
            service.addUserProfile(userProfile);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(resources.getString("dialogs.info"));
            alert.setHeaderText(resources.getString("dialogs.registered.header"));
            alert.setContentText(resources.getString("dialogs.registered.content"));
            alert.showAndWait();
            mainController.showEditProfile(userProfile);
        }
    }

    @FXML
    private void cancel(ActionEvent event) {
        mainController.goBack();
    }

    @Override
    public void clear() {
        usernameField.clear();
        passwordField.clear();
        avatarController.clear();
    }
}
