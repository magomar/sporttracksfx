package net.deludobellico.stfx.app.model;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 21/07/2016.
 */
public abstract class AbstractDataWrapper<T> implements DataWrapper<T> {
    T data;

    public AbstractDataWrapper(T data) {
        this.data = data;
    }

    @Override
    public T getData() {
        return data;
    }

    @Override
    public int hashCode() {
        return data.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return data.equals((T) obj);
    }
}
