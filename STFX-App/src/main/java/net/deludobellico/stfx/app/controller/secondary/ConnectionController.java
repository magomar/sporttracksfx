
package net.deludobellico.stfx.app.controller.secondary;

import com.jfoenix.controls.JFXToggleButton;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import net.deludobellico.stfx.app.model.UserAccountWrapper;
import net.deludobellico.stfx.connections.SportApp;
import net.deludobellico.stfx.model.ExternalApp;
import net.deludobellico.stfx.model.UserAccount;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 * Created on: 20/07/2016
 */
public class ConnectionController extends AbstractSecondaryController {

    @FXML
    private ImageView logoBack;
    @FXML
    private ImageView logoFront;
    @FXML
    private JFXToggleButton connectionButton;

    private ExternalApp externalApp;
    private UserAccountWrapper userAccount;


    public ExternalApp getExternalApp() {
        return externalApp;
    }

    public UserAccountWrapper getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = new UserAccountWrapper(userAccount);
        this.externalApp = SportApp.valueOf(userAccount.getApp());
        logoBack.setImage(externalApp.getLogo());
        logoFront.setImage(externalApp.getLogo());
        bindings();
    }

    private void bindings() {
        userAccount.connectedProperty().bind(connectionButton.selectedProperty());
        userAccount.connectedProperty().bind(logoFront.visibleProperty());
        userAccount.connectedProperty().bind(logoBack.visibleProperty().not());
    }
}
