package net.deludobellico.stfx.app.controller;

import com.google.inject.Injector;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import net.deludobellico.stfx.app.controller.secondary.*;
import net.deludobellico.stfx.model.UserProfile;
import net.deludobellico.stfx.persistence.dao.Dao;
import net.deludobellico.stfx.persistence.service.StfxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
@Singleton
public final class MainController implements Initializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);
    private final Map<ControlledView, Parent> views = new HashMap<>(ControlledView.values().length);
    private final Map<ControlledView, SecondaryController> controllers = new HashMap<>(ControlledView.values().length);
    @Inject
    private Injector injector;
    @Inject
    private StfxService service;
    @Inject
    @Named("i18n-resources")
    private ResourceBundle resources;

    @FXML
    private VBox root;
    @FXML
    private StackPane stackPane;
    @FXML
    private Label appTitle;

    private ControlledView currentView;
    private ControlledView previousView;
    private Stage primaryStage;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        appTitle.setCursor(Cursor.HAND);
        appTitle.setOnDragDetected(event -> appTitle.setCursor(Cursor.MOVE));
        appTitle.setOnMouseReleased(event -> appTitle.setCursor(Cursor.HAND));

    }

    public void start() {
        primaryStage = (Stage) stackPane.getScene().getWindow();
        List<UserProfile> userProfiles = service.getAllUserProfiles();
        if (userProfiles.isEmpty()) showRegisterView();
        else showSelectUserView(userProfiles);
    }

    public void showRegisterView() {
        showView(ControlledView.REGISTER_USER);
    }

    public void showSelectUserView(List<UserProfile> userProfiles) {
        loadView(ControlledView.SELECT_USER);
        ((SelectUserController) controllers.get(ControlledView.SELECT_USER)).setUserProfiles(userProfiles);
        showView(ControlledView.SELECT_USER);
    }

    public void showLoginView(UserProfile userProfile) {
        loadView(ControlledView.LOGIN);
        ((LoginController) controllers.get(ControlledView.LOGIN)).setUser(userProfile);
        showView(ControlledView.LOGIN);
    }

    public void showDashboard(UserProfile userProfile) {
        loadView(ControlledView.DASHBOARD);
        ((DashboardController) controllers.get(ControlledView.DASHBOARD)).setUserProfile(userProfile);
        showView(ControlledView.DASHBOARD);
    }

    public void showEditProfile(UserProfile userProfile) {
        loadView(ControlledView.EDIT_PROFILE);
        ((EditProfileController) controllers.get(ControlledView.EDIT_PROFILE)).setUserProfile(userProfile);
        showView(ControlledView.EDIT_PROFILE);
    }

    private void loadView(ControlledView view) {
        if (views.containsKey(view)) return;
        else {
            FXMLLoader fxmlLoader = injector.getInstance(FXMLLoader.class);
            fxmlLoader.setResources(resources);
            fxmlLoader.setLocation(getClass().getResource(view.fxmlResourcePath));
            try {
                Parent root = fxmlLoader.load();
                root.setOpacity(0);
                views.put(view, root);
                SecondaryController secondaryController = fxmlLoader.getController();
                controllers.put(view, secondaryController);
                stackPane.getChildren().add(root);
                LOGGER.info("View {} loaded.", view.name());
            } catch (IOException e) {
                LOGGER.error("Error loading view {};{}.", view.name(), e.getMessage());
            }
        }
    }

    private void showView(ControlledView view) {
        if (!views.containsKey(view)) loadView(view);
        if (currentView != null) views.get(currentView).setOpacity(0);
        previousView = currentView;
        currentView = view;
        Parent node = views.get(view);
        primaryStage.setWidth(view.preferredWitdh);
        primaryStage.setHeight(view.preferredHeight);
        stackPane.getChildren().clear();
        stackPane.getChildren().add(node);
        node.setOpacity(1);
        LOGGER.info("View {} shown.", view.name());
    }

    public void goBack() {
        if (null != previousView) showView(previousView);
        else {
//            YesNoAlert alert = injector.getInstance(YesNoAlert.class);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(resources.getString("dialogs.warn"));
            alert.setHeaderText(resources.getString("dialogs.quit.header"));
            alert.setContentText(resources.getString("dialogs.quit.content"));
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                Platform.exit();
            }
//            if (result.get() == alert.getButtonTypeYes()){
//                Platform.exit();
//            }
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    private enum ControlledView {
        REGISTER_USER("RegisterUser.fxml", 700, 400),
        SELECT_USER("SelectUser.fxml", 800, 400),
        LOGIN("Login.fxml", 400, 400),
        EDIT_PROFILE("EditProfile.fxml", 960, 800),
        DASHBOARD("Dashboard.fxml", 1280, 800);
        private final String fxmlResourcePath;
        private final double preferredWitdh;
        private final double preferredHeight;

        ControlledView(String fxmlFilename, double preferredWitdh, double preferredHeight) {
            fxmlResourcePath = "/fxml/" + fxmlFilename;
            this.preferredWitdh = preferredWitdh;
            this.preferredHeight = preferredHeight;
        }
    }

//    private <T extends  WindowController> T init(String viewUrl, WindowController<T> controller) {
//        return controller.init(viewUrl, primaryStage, MainController.this);
//    }
}
