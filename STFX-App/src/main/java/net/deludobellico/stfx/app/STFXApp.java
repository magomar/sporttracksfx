package net.deludobellico.stfx.app;

import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.deludobellico.stfx.app.controller.MainController;
import net.deludobellico.stfx.app.guice.STFXModule;
import org.aeonbits.owner.ConfigCache;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class STFXApp extends Application {
    private double xOffset = 0;
    private double yOffset = 0;



    @Override
    public void start(Stage primaryStage) {
        // Obtain a Guice injector
        Injector injector = Guice.createInjector(new STFXModule());

        // Start the Jpa Persistence Service (and extension of Google Guice), see STFXModule: new JpaPersistModule()
//        injector.getInstance(PersistServiceInitializer.class);

        //The FXMLLoader is instantiated in a separated Guice Provider<FXMLLoader> called FXMLLoaderProvider.
        FXMLLoader fxmlLoader = injector.getInstance(FXMLLoader.class);
        // Get AppConfig properties
        AppConfig appConfig = ConfigCache.getOrCreate(AppConfig.class);
        // Set global Css style
//        Application.setUserAgentStylesheet(getClass().getResource(appConfig.defaultCss()).toExternalForm());
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("i18n.messages");
            fxmlLoader.setLocation(getClass().getResource("/fxml/Main.fxml"));
            fxmlLoader.setResources(bundle);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(STFXApp.class.getResource("/css/jfoenix-fonts.css").toExternalForm());
            scene.getStylesheets().add(STFXApp.class.getResource("/css/jfoenix-design.css").toExternalForm());
            scene.getStylesheets().add(STFXApp.class.getResource("/css/jfoenix-main-demo.css").toExternalForm());

            primaryStage.setScene(scene);
            primaryStage.setMinWidth(appConfig.minWidth());
            primaryStage.setMinHeight(appConfig.minHeight());
            primaryStage.initStyle(StageStyle.UNDECORATED);

            root.setOnMousePressed(event -> {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            });
            root.setOnMouseDragged(event -> {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            });
            root.setOnDragDetected(event -> scene.setCursor(Cursor.MOVE));
            root.setOnMouseReleased(event -> scene.setCursor(null));

            MainController mainController = fxmlLoader.getController();
            mainController.start();

            primaryStage.show();
            primaryStage.setOnCloseRequest(e -> {
                System.exit(0);
            });

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
