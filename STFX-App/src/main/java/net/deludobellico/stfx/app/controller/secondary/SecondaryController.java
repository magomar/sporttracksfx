package net.deludobellico.stfx.app.controller.secondary;

import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;


/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public interface SecondaryController extends Initializable {
    default void clear() {
    }

    @Override
    default void initialize(URL url, ResourceBundle rb) {
    }
}
