package net.deludobellico.stfx.app.engine;

import net.deludobellico.stfx.model.*;
import net.deludobellico.stfx.model.enums.ActivityType;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.TemporalAmount;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Mario on 15/07/2016.
 */
public class StatisticsEngine {
    private static final TemporalAmount FOUR_WEEKS = Period.ofDays(28);
    private static final TemporalAmount THREE_MONTHS = Period.ofMonths(3);
    private static final TemporalAmount SIX_MONTHS = Period.ofMonths(6);

    public static StatisticsEntry computeStatistics(List<Activity> activities) {
        StatisticsEntry stats = new StatisticsEntry();
        stats.setCount(activities.size());
        double distance = 0;
        int movingTime = 0;
        int duration = 0;
        double ascent = 0;
        for (Activity activity : activities) {
            distance += activity.getDistance();
            duration += activity.getDistance();
            ascent += activity.getAscent();
        }
        stats.setDistance(distance);
        stats.setDuration(duration);
        stats.setMovingTime(movingTime);
        stats.setAscent(ascent);
        return stats;
    }

    public static UserStatistics computeUserStatistics(UserProfile userProfile) {
        UserStatistics stats = new UserStatistics();
        List<Activity> activities = userProfile.getActivities();

        // Global statistics
        double biggestRideDistance = 0;
        double biggestAscent =0;
        for (Activity activity : activities) {
            biggestRideDistance = Math.max(biggestRideDistance, activity.getDistance());
            biggestAscent = Math.max(biggestAscent, activity.getAscent());
        }
        stats.setBiggestRideDistance(biggestRideDistance);
        stats.setBiggestAscent(biggestAscent);
        List<Activity> allRuns  = activities.stream().filter(new ActivityFilter(ActivityType.RUN)).collect(Collectors.toList());
        List<Activity> allRides  = activities.stream().filter(new ActivityFilter(ActivityType.RIDE)).collect(Collectors.toList());
        List<Activity> allSwims  = activities.stream().filter(new ActivityFilter(ActivityType.SWIM)).collect(Collectors.toList());
        stats.setAllRideTotals(computeStatistics(allRuns));
        stats.setAllRideTotals(computeStatistics(allRides));
        stats.setAllRideTotals(computeStatistics(allSwims));

        // Recent statistics (4 weeks)
        List<Activity> recentRuns  = activities.stream().filter(new ActivityFilter(ActivityType.RUN, FOUR_WEEKS)).collect(Collectors.toList());
        List<Activity> recentRides  = activities.stream().filter(new ActivityFilter(ActivityType.RIDE, FOUR_WEEKS)).collect(Collectors.toList());
        List<Activity> recentSwims  = activities.stream().filter(new ActivityFilter(ActivityType.SWIM, FOUR_WEEKS)).collect(Collectors.toList());
        stats.setRecentRideTotals(computeStatistics(recentRuns));
        stats.setRecentRideTotals(computeStatistics(recentRides));
        stats.setRecentRideTotals(computeStatistics(recentSwims));


        // Year to date statistics
        int currentYear = LocalDate.now().getYear();
        LocalDate firstDayOfCurrentYear = LocalDate.of(currentYear, 1, 1);
        List<Activity> ytdRuns  = activities.stream().filter(new ActivityFilter(ActivityType.RUN, firstDayOfCurrentYear)).collect(Collectors.toList());
        List<Activity> ytdRides  = activities.stream().filter(new ActivityFilter(ActivityType.RIDE, firstDayOfCurrentYear)).collect(Collectors.toList());
        List<Activity> ytdSwims  = activities.stream().filter(new ActivityFilter(ActivityType.SWIM, firstDayOfCurrentYear)).collect(Collectors.toList());
        stats.setYtdRunTotals(computeStatistics(ytdRuns));
        stats.setYtdRunTotals(computeStatistics(ytdRides));
        stats.setYtdRunTotals(computeStatistics(ytdSwims));




        return stats;
    }
}
