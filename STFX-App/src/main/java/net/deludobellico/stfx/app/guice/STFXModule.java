package net.deludobellico.stfx.app.guice;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import javafx.fxml.FXMLLoader;
import net.deludobellico.stfx.persistence.PersistenceConfig;
import net.deludobellico.stfx.persistence.dao.Dao;
import net.deludobellico.stfx.persistence.service.ServiceImpl;
import net.deludobellico.stfx.persistence.service.StfxService;
import org.aeonbits.owner.ConfigCache;

import javax.persistence.EntityManager;
import java.util.ResourceBundle;

//import com.google.inject.persist.jpa.JpaPersistModule;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 13/07/2016.
 */
public class STFXModule extends AbstractModule {
    private Class<? extends Dao> daoClass;
//    private final String persistenceUnitName;

    public STFXModule() {
        PersistenceConfig config = ConfigCache.getOrCreate(PersistenceConfig.class);
//        persistenceUnitName = config.persistenceUnitName();
        try {
            daoClass = (Class<? extends Dao>) Class.forName(config.persistenceFramework());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    protected void configure() {
        bind(FXMLLoader.class).toProvider(FXMLLoaderProvider.class);
//        install(new JpaPersistModule(persistenceUnitName));
        bind(EntityManager.class).toProvider(EntityManagerProvider.class); //alternative to installing JpaPersistModule
        bind(ResourceBundle.class).annotatedWith(Names.named("i18n-resources"))
                .toInstance(ResourceBundle.getBundle("i18n.messages"));
        bind(ResourceBundle.class).annotatedWith(Names.named("i18n-enums"))
                .toInstance(ResourceBundle.getBundle("i18n.enums"));
        bind(Dao.class).to(daoClass);
        bind(StfxService.class).to(ServiceImpl.class);
    }
}
