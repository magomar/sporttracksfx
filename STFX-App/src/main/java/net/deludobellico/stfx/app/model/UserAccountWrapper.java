package net.deludobellico.stfx.app.model;

import javafx.beans.property.*;
import net.deludobellico.stfx.model.UserAccount;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 21/07/2016.
 */
public class UserAccountWrapper extends AbstractDataWrapper<UserAccount> {
    private StringProperty app;
    private StringProperty username;
    private StringProperty password;
    private BooleanProperty connected;
    private BooleanProperty download;
    private BooleanProperty upload;

    public UserAccountWrapper(String app) {
        super(new UserAccount(app));
        this.app = new ReadOnlyStringWrapper(app);
        username = new SimpleStringProperty();
        password = new SimpleStringProperty();
        connected = new SimpleBooleanProperty();
        download = new SimpleBooleanProperty();
        upload = new SimpleBooleanProperty();
    }

    public UserAccountWrapper(UserAccount data) {
        super(data);
        app = new ReadOnlyStringWrapper(data.getApp());
        username = new SimpleStringProperty(data.getUsername());
        password = new SimpleStringProperty(data.getPassword());
        connected = new SimpleBooleanProperty(data.isConnected());
        download = new SimpleBooleanProperty(data.isDownload());
        upload = new SimpleBooleanProperty(data.isUpload());
    }

    @Override
    public void update() {
        data.setUsername(username.get());
        data.setPassword(password.get());
        data.setConnected(connected.get());
        data.setDownload(download.get());
        data.setUpload(upload.get());
    }

    public String getApp() {
        return app.get();
    }

    public String getUsername() {
        return username.get();
    }

    public StringProperty usernameProperty() {
        return username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public boolean isConnected() {
        return connected.get();
    }

    public BooleanProperty connectedProperty() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected.set(connected);
    }

    public boolean isDownload() {
        return download.get();
    }

    public BooleanProperty downloadProperty() {
        return download;
    }

    public void setDownload(boolean download) {
        this.download.set(download);
    }

    public boolean isUpload() {
        return upload.get();
    }

    public BooleanProperty uploadProperty() {
        return upload;
    }

    public void setUpload(boolean upload) {
        this.upload.set(upload);
    }
}
