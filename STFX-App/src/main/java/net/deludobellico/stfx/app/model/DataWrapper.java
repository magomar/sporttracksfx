package net.deludobellico.stfx.app.model;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 * Created on 21/07/2016.
 */
public interface DataWrapper<T> {
    void update();
    T getData();
}
