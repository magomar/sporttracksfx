package net.deludobellico.stfx.app;

import org.aeonbits.owner.Config;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 14/07/2016.
 */
public interface AppConfig extends Config {

    @Config.DefaultValue("SporTracksFX")
    @Config.Key("app.title")
    String appTitle();

    @Config.DefaultValue("/css/stfx-jfoenix.css")
    @Config.Key("app.gui.stfxCss")
    String stfxCss();

    @Config.DefaultValue("/css/material-stfx-jfoenix.css")
    @Config.Key("app.gui.defaultCss")
    String defaultCss();

    @Config.DefaultValue("400")
    @Config.Key("app.gui.minWidth")
    double minWidth();

    @Config.DefaultValue("400")
    @Config.Key("app.gui.minHeight")
    double minHeight();

    @Config.DefaultValue("/images/default_avatar_male.png")
    @Config.Key("app.gui.defaultAvatar")
    String defaultAvatar();
}
