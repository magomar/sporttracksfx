package net.deludobellico.stfx.app.controller.included;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import net.deludobellico.stfx.app.AppConfig;
import net.deludobellico.stfx.app.controller.secondary.AbstractSecondaryController;
import net.deludobellico.stfx.model.UserProfile;
import org.aeonbits.owner.ConfigCache;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class AvatarController extends AbstractSecondaryController {
    @FXML
    private VBox root;
    @FXML
    private ImageView avatar;
    @FXML
    private Label username;
    @FXML
    private JFXButton editButton;
    @FXML
    private JFXButton removeButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        clear();
        avatar.setClip(new Circle(86, 86, 86));
        setMode(AvatarMode.VIEW);
    }

    public void setUserProfile(UserProfile userProfile) {
        username.setText(userProfile.getUsername());
        if (userProfile.getAvatar() != null)
            avatar.setImage(userProfile.getAvatar().getImage());
    }

    public void setMode(AvatarMode mode) {
        switch (mode) {
            case VIEW:
                editButton.setVisible(false);
                removeButton.setVisible(false);
                username.setVisible(true);
                break;
            case EDIT:
                editButton.setVisible(true);
                removeButton.setVisible(true);
                username.setVisible(false);
                break;
        }
    }

    public Image getAvatar() {
        return avatar.getImage();
    }

    @FXML
    private void selectAvatarImage(ActionEvent event) throws MalformedURLException {
        FileChooser chooser = new FileChooser();
        chooser.setTitle(resources.getString("avatar.chooseImage"));
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files",
                        "*.bmp", "*.png", "*.jpg", "*.gif")); // limit chooser options to image files
        File file = chooser.showOpenDialog(mainController.getPrimaryStage());
        if (file != null) {
            String imagepath = file.toURI().toURL().toString();
            Image image = new Image(imagepath);
            avatar.setImage(image);
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(resources.getString("dialogs.info"));
            alert.setHeaderText(resources.getString("dialogs.selectFile.header"));
            alert.setContentText(resources.getString("dialogs.selectFile.context"));
            alert.showAndWait();
        }
    }

    @FXML
    private void removeAvatarImage(ActionEvent event) {
        clear();
    }

    public void clear() {
        AppConfig appConfig = ConfigCache.getOrCreate(AppConfig.class);
        Image image = new Image(getClass().getResourceAsStream(appConfig.defaultAvatar()));
        avatar.setImage(image);
    }

    public enum AvatarMode {
        EDIT,
        VIEW;
    }
}
