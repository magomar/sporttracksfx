package net.deludobellico.stfx.app.controller.other;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 13/07/2016.
 */
public class YesNoAlert extends Alert {
    private ButtonType buttonTypeYes;
    private ButtonType buttonTypeNo;

    @Inject
    public YesNoAlert( @Named("i18n-resources") ResourceBundle resources) {
        super(AlertType.CONFIRMATION);
        buttonTypeYes = new ButtonType(resources.getString("button.yes"));
        buttonTypeNo = new ButtonType(resources.getString("button.no"), ButtonBar.ButtonData.CANCEL_CLOSE);
        getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
    }

    public ButtonType getButtonTypeYes() {
        return buttonTypeYes;
    }

    public ButtonType getButtonTypeNo() {
        return buttonTypeNo;
    }
}
