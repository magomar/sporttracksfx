package net.deludobellico.stfx.app.controller.secondary;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Shadow;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import net.deludobellico.stfx.app.controller.included.AvatarController;
import net.deludobellico.stfx.model.UserProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class SelectUserController extends AbstractSecondaryController {
    private final static Logger LOGGER = LoggerFactory.getLogger(SelectUserController.class);
    private static final DropShadow SHADOW = new DropShadow();

    @FXML
    private HBox usersPane;

    @FXML
    private void registerNewUser(ActionEvent event) {
        mainController.showRegisterView();
    }

    public void setUserProfiles(List<UserProfile> userProfiles) {
        clear();
        for (UserProfile userProfile : userProfiles) {
            addUserProfile(userProfile);
        }
    }

    private void addUserProfile(UserProfile userProfile) {
        FXMLLoader fxmlLoader = getFXMLLoader("/fxml/Avatar.fxml", resources);
        try {
            Parent root = fxmlLoader.load();
            AvatarController avatarController = fxmlLoader.getController();
            avatarController.setUserProfile(userProfile);
            usersPane.getChildren().add(root);
            root.setOnMouseEntered(event -> root.setEffect(SHADOW));
            root.setOnMouseExited(event -> root.setEffect(null));
            root.setOnMouseClicked(event -> mainController.showLoginView(userProfile));
        } catch (IOException e) {
            LOGGER.error("Error loading Avatar view for user {}", userProfile.getUsername());
        }
    }

    @Override
    public void clear() {
        usersPane.getChildren().clear();
    }
}
