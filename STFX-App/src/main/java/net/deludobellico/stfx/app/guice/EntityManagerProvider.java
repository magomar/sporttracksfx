package net.deludobellico.stfx.app.guice;

import net.deludobellico.stfx.persistence.PersistenceConfig;
import org.aeonbits.owner.ConfigCache;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 23/07/2016.
 */
public class EntityManagerProvider implements Provider<EntityManager> {

    private String persistenceUniName;

    public EntityManagerProvider() {
        PersistenceConfig config = ConfigCache.getOrCreate(PersistenceConfig.class);
        this.persistenceUniName = config.persistenceUnitName();
    }

    @Override
    public EntityManager get() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUniName);
        EntityManager em = emf.createEntityManager();
        return em;
    }
}
