package net.deludobellico.stfx.app.guice;


import com.google.inject.Injector;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.util.BuilderFactory;

import javax.inject.Inject;
import javax.inject.Provider;

public class FXMLLoaderProvider implements Provider<FXMLLoader> {
    private static final BuilderFactory builderFactory = new JavaFXBuilderFactory();

    @Inject
    Injector injector;

    @Override
    public FXMLLoader get() {
        FXMLLoader loader = new FXMLLoader();
        loader.setControllerFactory(param -> injector.getInstance(param));
        loader.setBuilderFactory(builderFactory);
        return loader;
    }

}
