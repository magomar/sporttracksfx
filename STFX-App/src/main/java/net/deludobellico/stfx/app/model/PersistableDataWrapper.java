package net.deludobellico.stfx.app.model;

import net.deludobellico.stfx.persistence.dao.Dao;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 21/07/2016.
 */
public interface PersistableDataWrapper<T> extends DataWrapper<T> {
    boolean persist(Dao dao);
    boolean updateAndPersist(Dao dao);
}
