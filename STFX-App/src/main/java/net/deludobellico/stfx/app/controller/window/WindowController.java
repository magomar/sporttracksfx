package net.deludobellico.stfx.app.controller.window;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.deludobellico.stfx.app.controller.MainController;
import net.deludobellico.stfx.app.controller.secondary.SecondaryController;

import java.io.IOException;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public interface WindowController<T extends WindowController> extends SecondaryController {

    Stage getStage();

    T init(String viewUrl, Stage owner, MainController mainController);
}
