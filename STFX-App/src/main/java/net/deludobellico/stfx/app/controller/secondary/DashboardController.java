package net.deludobellico.stfx.app.controller.secondary;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import net.deludobellico.stfx.app.engine.StatisticsEngine;
import net.deludobellico.stfx.model.UserProfile;
import net.deludobellico.stfx.model.UserStatistics;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class DashboardController extends AbstractSecondaryController {
    @FXML
    private HBox root;
    @FXML
    private ImageView avatar;
    @FXML
    private Label username;
    @FXML
    private Label actualMonthLabel;
    @FXML
    private Label actualMonthActivities;
    @FXML
    private HBox distancepane;
    @FXML
    private Label actualMonthDistance;
    @FXML
    private HBox durationpane;
    @FXML
    private Label actualMonthDuration;
    @FXML
    private HBox vmediapane;
    @FXML
    private Label actualMonthSpeed;
    @FXML
    private Label lastDateLabel1;
    @FXML
    private VBox lastActPane2;
    @FXML
    private Label totalDistanceYear;
    @FXML
    private VBox lastActPane3;
    @FXML
    private Label totalDurationYear;
    @FXML
    private VBox lastActPane4;
    @FXML
    private Label totalVMYear;
    @FXML
    private BarChart<?, ?> barChart;
    @FXML
    private NumberAxis yAxis;
    @FXML
    private CategoryAxis xAxis;
    @FXML
    private JFXButton editProfile;


    private UserProfile userProfile;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }


    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
        username.setText(userProfile.getUsername());
        avatar.setImage(userProfile.getAvatar().getImage());
        UserStatistics userStats = StatisticsEngine.computeUserStatistics(userProfile);
    }

    @FXML
    private void openActivitiesView(MouseEvent event) {
    }

    @FXML
    private void loadCustomTrack(MouseEvent event) {
    }

    @FXML
    private void lastWeekActivitiesHover(MouseEvent event) {
    }

    @Override
    public void clear() {

    }

    @FXML
    private void editProfile(ActionEvent event) {
        mainController.showEditProfile(userProfile);
    }

}
