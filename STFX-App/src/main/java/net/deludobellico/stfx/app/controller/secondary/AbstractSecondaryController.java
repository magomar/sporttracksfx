package net.deludobellico.stfx.app.controller.secondary;

import com.google.inject.Injector;
import javafx.fxml.FXMLLoader;
import net.deludobellico.stfx.app.controller.MainController;
import net.deludobellico.stfx.persistence.service.StfxService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */

public abstract class AbstractSecondaryController implements SecondaryController {
    @Inject
    protected Injector injector;

    @Inject
    protected StfxService service;

    @Inject
    @Named("i18n-resources")
    protected ResourceBundle resources;

    @Inject
    protected MainController mainController;

    protected FXMLLoader getFXMLLoader(String fxml, ResourceBundle resources) {
        FXMLLoader fxmlLoader = injector.getInstance(FXMLLoader.class);
        fxmlLoader.setLocation(getClass().getResource(fxml));
        fxmlLoader.setResources(resources);
        return fxmlLoader;
    }
}
