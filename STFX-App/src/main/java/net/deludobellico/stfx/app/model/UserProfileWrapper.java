package net.deludobellico.stfx.app.model;

import javafx.beans.property.*;
import javafx.beans.property.adapter.JavaBeanObjectProperty;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.deludobellico.stfx.model.PersistentImage;
import net.deludobellico.stfx.model.UserProfile;
import net.deludobellico.stfx.model.enums.MeasurementMethod;
import net.deludobellico.stfx.model.enums.Sex;
import net.deludobellico.stfx.persistence.dao.Dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Map.Entry;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 22/07/2016.
 */
public final class UserProfileWrapper extends AbstractPersistableDataWrapper<UserProfile> {
    /**
     * User's name used to log-in
     */
    private StringProperty username = new SimpleStringProperty();
    /**
     * User's password used to log-in
     */
    private StringProperty password = new SimpleStringProperty();
    /**
     * DB persisted image representing the user
     */
    private ObjectProperty<PersistentImage> avatar = new SimpleObjectProperty<>();
    /**
     * User's sex
     */
    private ObjectProperty<Sex> sex = new SimpleObjectProperty<>();
    /**
     * User's date of birth
     */
    private ObjectProperty<LocalDate> birthday = new SimpleObjectProperty<>();
    /**
     * User's weight (in kilograms)
     */
    private DoubleProperty weight = new SimpleDoubleProperty() {
    };
    /**
     * User's first name
     */
    private StringProperty firstname = new SimpleStringProperty();
    /**
     * User's last name
     */
    private StringProperty lastname = new SimpleStringProperty();
    /**
     * City the User lives in
     */
    private StringProperty city = new SimpleStringProperty();
    /**
     * State, county, canton etc that the User lives in
     */
    private StringProperty state = new SimpleStringProperty();
    /**
     * Country the User lives in
     */
    private StringProperty country = new SimpleStringProperty();
    /**
     * User's email address
     */
    private StringProperty email = new SimpleStringProperty();
    /**
     * Date format preference
     */
    private ObjectProperty<DateFormat> datePreference = new SimpleObjectProperty<>();
    /**
     * Measurement preference (metric or imperial)
     */
    private ObjectProperty<MeasurementMethod> measurementPreference = new SimpleObjectProperty<>();


    private ObservableList<ActivityWrapper> activities = FXCollections.observableArrayList();

    private Map<String, UserAccountWrapper> accounts = FXCollections.observableHashMap();

    public UserProfileWrapper(String username, String password, PersistentImage image) {
        super(new UserProfile(username, password));
        this.username.set(username);
        this.password.set(password);
        this.avatar.set(image);
    }


    public UserProfileWrapper(UserProfile data) throws NoSuchMethodException {
        super(data);
        username.set(data.getUsername());
        password.set(data.getPassword());
        sex.set(data.getSex());
        if (null != data.getBirthday())
            birthday.set(data.getBirthday().toLocalDate());
        JavaBeanObjectProperty<LocalDate> objectProperty = JavaBeanObjectPropertyBuilder.create().build();
        birthday = JavaBeanObjectPropertyBuilder.create().bean(data.getBirthday()).name("birthday").build();
        weight.set(data.getWeight());
        if (null != data.getAvatar())
            avatar.set(data.getAvatar());
        firstname.set(data.getFirstname());
        lastname.set(data.getLastname());
        city.set(data.getCity());
        state.set(data.getState());
        country.set(data.getCountry());
        datePreference.set(new SimpleDateFormat(data.getDatePreference()));
        measurementPreference.set(data.getMeasurementPreference());
        activities.addAll(data.getActivities().parallelStream().map(ActivityWrapper::new).collect(Collectors.toList()));
        accounts.putAll(data.getAccounts().entrySet().stream()
                .collect(Collectors.toMap(Entry::getKey, entry -> new UserAccountWrapper(entry.getValue()))));
    }

    @Override
    public void update() {
        data.setUsername(username.get());
        data.setPassword(password.get());
        data.setSex(sex.get());
        if (null != birthday.get())
            data.setBirthday(java.sql.Date.valueOf(birthday.get()));
        data.setWeight(weight.get());
        if (null != avatar)
            data.setAvatar(avatar.get());
        data.setSex(sex.get());


    }

    @Override
    public boolean persist(Dao dao) {
        return false;
    }
}
