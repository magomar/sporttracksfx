package net.deludobellico.stfx.app.controller.secondary;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;
import net.deludobellico.stfx.app.controller.included.AvatarController;
import net.deludobellico.stfx.connections.SportApp;
import net.deludobellico.stfx.model.PersistentImage;
import net.deludobellico.stfx.model.enums.Sex;
import net.deludobellico.stfx.model.UserAccount;
import net.deludobellico.stfx.model.UserProfile;
import net.deludobellico.stfx.persistence.service.ServiceException;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.EnumMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 *
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 * Created on 20/07/2016
 */
public class EditProfileController extends AbstractSecondaryController {
    private static final StringConverter<Double> DOUBLE_STRING_CONVERTER = new DoubleStringConverter();

    @FXML
    private HBox root;
    @FXML
    private VBox connectionsPane;
    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField oldPassword;
    @FXML
    private JFXComboBox<Sex> sex;
    @FXML
    private JFXTextField weight;
    @FXML
    private JFXTextField country;
    @FXML
    private JFXTextField firstname;
    @FXML
    private JFXDatePicker birthday;
    @FXML
    private JFXPasswordField newPassword;
    @FXML
    private JFXPasswordField newPassword2;

    private UserProfile userProfile;

    private AvatarController avatarController;

    private Map<SportApp, ConnectionController> connectionControllers = new EnumMap<>(SportApp.class);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        username.setEditable(false);
        FXMLLoader fxmlLoader = getFXMLLoader("/fxml/Avatar.fxml", resources);
        try {
            Parent child = fxmlLoader.load();
            avatarController = fxmlLoader.getController();
            avatarController.setMode(AvatarController.AvatarMode.EDIT);
            root.getChildren().add(0, child);
            for (SportApp app : SportApp.values()) {
                fxmlLoader = getFXMLLoader("/fxml/Connection.fxml", resources);
                child = fxmlLoader.load();
                ConnectionController connectionController = fxmlLoader.getController();
                connectionControllers.put(app, connectionController);
                connectionsPane.getChildren().add(child);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        sex.getItems().addAll(Sex.values());
//        clear();
    }

    @FXML
    private void saveChanges(ActionEvent event) {
        userProfile.setFirstname(firstname.getText());
        userProfile.setAvatar(new PersistentImage(avatarController.getAvatar()));
        userProfile.setSex(sex.getValue());
        if (null != birthday.getValue())
            userProfile.setBirthday(Date.valueOf(birthday.getValue()));
        userProfile.setCountry(country.getText());
        if (!weight.getText().isEmpty())
            userProfile.setWeight(DOUBLE_STRING_CONVERTER.fromString(weight.getText()));
        try {
            service.updateUserProfile(userProfile);
        } catch (ServiceException e) {
            //TODO show a dialog informing of the problem
            e.printStackTrace();
        }
        mainController.goBack();
    }

    @FXML
    private void cancel(ActionEvent event) {
        mainController.goBack();
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
        username.setText(userProfile.getUsername());
        firstname.setText(userProfile.getFirstname());
        avatarController.setUserProfile(userProfile);
        sex.getSelectionModel().select(userProfile.getSex());
//        weight.setText(String.format("%.2f", userProfile.getWeight()));
        weight.setText(DOUBLE_STRING_CONVERTER.toString(userProfile.getWeight()));
        if (null != userProfile.getBirthday())
            birthday.setValue(userProfile.getBirthday().toLocalDate());
        else birthday.setValue(null);
        country.setText(userProfile.getCountry());
        for (Map.Entry<SportApp, ConnectionController> entry : connectionControllers.entrySet()) {
            SportApp app = entry.getKey();
            ConnectionController controller = entry.getValue();
            if (userProfile.getAccounts().containsKey(app.getName()))
                controller.setUserAccount(userProfile.getAccounts().get(app.getName()));
            else {
                UserAccount newUserAccount = new UserAccount(app.getName());
                controller.setUserAccount(newUserAccount);
                userProfile.getAccounts().put(app.getName(), newUserAccount);
            }
        }
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    @Override
    public void clear() {
        this.userProfile = null;
        firstname.setText("");
        avatarController.clear();
        sex.getSelectionModel().clearSelection();
        weight.setText("0.0");
        birthday.setValue(LocalDate.now());
        country.setText("");
    }
}
