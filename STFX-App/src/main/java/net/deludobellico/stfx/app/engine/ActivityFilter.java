package net.deludobellico.stfx.app.engine;

import net.deludobellico.stfx.model.Activity;
import net.deludobellico.stfx.model.enums.ActivityType;

import java.time.LocalDate;
import java.time.temporal.TemporalAmount;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Created by Mario on 15/07/2016.
 */
public class ActivityFilter implements Predicate<Activity> {

    private final Predicate<Activity> compositePredicate;

    private static final Map<ActivityType, ActivityTypeFilter> activityTypePredicate =
            new EnumMap<>(ActivityType.class);


    public ActivityFilter(ActivityType type) {
        compositePredicate = getOrCreateActivityTypeFilter(type);
    }

    public ActivityFilter(ActivityType type, LocalDate oldestDate) {
        compositePredicate = getOrCreateActivityTypeFilter(type).
                and(new DateFilter(oldestDate));
    }

    public ActivityFilter(ActivityType type, TemporalAmount antiquity) {
        compositePredicate = getOrCreateActivityTypeFilter(type).
                and(new DateFilter(antiquity));
    }

    @Override
    public boolean test(Activity t) {
        return compositePredicate.test(t);
    }


    private class DateFilter implements Predicate<Activity> {
        private final LocalDate oldestDate;

        DateFilter(LocalDate oldestDate) {
            this.oldestDate = oldestDate;
        }

        DateFilter(TemporalAmount antiquity) {
            this.oldestDate = LocalDate.now().minus(antiquity);
        }

        @Override
        public boolean test(Activity a) {
                return a.getLocalDate().isAfter(oldestDate);
        }
    }

    private static class ActivityTypeFilter implements Predicate<Activity> {
        private final ActivityType type;

        public ActivityTypeFilter(ActivityType type) {
            this.type = type;
        }

        @Override
        public boolean test(Activity activity) {
            return type.equals(activity.getType());
        }

    }

    public static ActivityTypeFilter getOrCreateActivityTypeFilter(ActivityType activityType) {
        ActivityTypeFilter activityTypeFilter;
        if (activityTypePredicate.containsKey(activityType)) {
            activityTypeFilter = activityTypePredicate.get(activityType);
        } else {
            activityTypeFilter = new ActivityTypeFilter(activityType);
            activityTypePredicate.put(activityType, activityTypeFilter);
        }
        return activityTypeFilter;
    }

}

