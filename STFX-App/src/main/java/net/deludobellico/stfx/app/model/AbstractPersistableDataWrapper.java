package net.deludobellico.stfx.app.model;

import net.deludobellico.stfx.persistence.dao.Dao;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 21/07/2016.
 */
public abstract class AbstractPersistableDataWrapper<T> extends AbstractDataWrapper<T> implements PersistableDataWrapper<T> {
    public AbstractPersistableDataWrapper(T data) {
        super(data);
    }

    @Override
    public boolean updateAndPersist(Dao dao) {
        update();
        return persist(dao);
    }
}
